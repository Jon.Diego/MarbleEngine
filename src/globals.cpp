#include "globals.h"
#include "game_object/game_object.h"//GameObject.
#include "game_object/components/camera/camera_controller.h"//CameraController
#include "game_object/components/camera/camera_lens.h"//CameraLens

namespace mrbl
{
	namespace globals
	{
		std::unique_ptr<GameObject> camera_ptr = nullptr;
		std::unique_ptr<GameObject> box_ptr = nullptr;
		void CreateInputActions()
		{
			mrbl::InputSystem& input_system = mrbl::InputSystem::get();
			input_system.add_input_action(strings::camera_movement::TurnUp, { mrbl::ButtonStates::Pressed, mrbl::DeviceTypes::KeyBoard, mrbl::Keys::Key_W });
			input_system.add_input_action(strings::camera_movement::TurnLeft, { mrbl::ButtonStates::Pressed, mrbl::DeviceTypes::KeyBoard, mrbl::Keys::Key_A });
			input_system.add_input_action(strings::camera_movement::TurnDown, { mrbl::ButtonStates::Pressed, mrbl::DeviceTypes::KeyBoard, mrbl::Keys::Key_S });
			input_system.add_input_action(strings::camera_movement::TurnRight, { mrbl::ButtonStates::Pressed, mrbl::DeviceTypes::KeyBoard, mrbl::Keys::Key_D });
			input_system.add_input_action(strings::camera_movement::Forward, { mrbl::ButtonStates::Pressed, mrbl::DeviceTypes::KeyBoard, mrbl::Keys::Key_SPACE });
			input_system.add_input_action(strings::camera_movement::Backward, { mrbl::ButtonStates::Pressed, mrbl::DeviceTypes::KeyBoard, mrbl::Keys::Key_MAYUS_L });
			input_system.add_input_action(strings::camera_movement::Up, { mrbl::ButtonStates::Pressed, mrbl::DeviceTypes::KeyBoard, mrbl::Keys::Key_UP });
			input_system.add_input_action(strings::camera_movement::Down, { mrbl::ButtonStates::Pressed, mrbl::DeviceTypes::KeyBoard, mrbl::Keys::Key_DOWN });
			input_system.add_input_action(strings::camera_movement::Left, { mrbl::ButtonStates::Pressed, mrbl::DeviceTypes::KeyBoard, mrbl::Keys::Key_LEFT });
			input_system.add_input_action(strings::camera_movement::Right, { mrbl::ButtonStates::Pressed, mrbl::DeviceTypes::KeyBoard, mrbl::Keys::Key_RIGHT });
		}

		void CreateGlobalObjects()
		{
			box_ptr = std::make_unique<GameObject>();
			camera_ptr = std::make_unique<GameObject>(glm::fvec3(0.f,0.f, 100.f));
			camera_ptr->add_component(std::make_unique<CameraController>());
			camera_ptr->add_component(std::make_unique<CameraLens>());
		}
	}
}