#include "exception/exception.h"
#include "logger/logger.h"
#include "media_layer/base/media_layer.h"
#include "media_layer/window_manager/window_manager.h"
#include "media_layer/input_system/input_system.h"
#include "framerate_controller/framerate_controller.h"
#include "resource_manager/resource_manager.h"
#include "game_systems/graphics_system/graphics_system.h"
#include "globals.h"//Global objects and functions for fast testing.


void init();
void loop();


int main(void)
{
	mrbl::Logger logger;
	try
	{
		//System Creation.
		mrbl::MediaLayer media_layer;
		mrbl::WindowManager w_manager;
		mrbl::InputSystem input_system;
		mrbl::FramerateController fr_controller(mrbl::FramerateController::frames_to_delta(30.f), mrbl::FramerateController::frames_to_delta(60.f), mrbl::FramerateController::frames_to_delta(20.f));
		mrbl::ResourceManager resource_manager;
		mrbl::Window * window = w_manager.create_window("main_window", 800, 600,mrbl::VSyncType::ADAPTATIVE_VSINC_IF_NOT_THEN_VSYNC);
		window->set_viewport(0, 0, 800, 600);
		w_manager.set_main_window(window, 0);
		mrbl::GraphicsSystem graphics_system;

		//Application Creation.
		init();
		//Main Loop
		while (input_system.pool_events())
		{
			loop();
			fr_controller.end_frame();
		}
	}
	catch(mrbl::Exception excep)
	{
		logger.write_and_flush(excep.what());
	}
}


void init()
{
	//Systems Init.
	mrbl::GraphicsSystem::get().init();

	//Action Creation.
	mrbl::globals::CreateInputActions();
}

void loop()
{
	mrbl::GraphicsSystem::get().update();
	mrbl::WindowManager::get().get_main_window(0)->swap_buffers();

	mrbl::InputSystem& input_sys = mrbl::InputSystem::get();
	namespace gstring = mrbl::globals::strings::camera_movement;
	if (input_sys.check_input_action(gstring::TurnUp))
		printf("%s\n", gstring::TurnUp);
	if (input_sys.check_input_action(gstring::TurnLeft))
		printf("%s\n", gstring::TurnLeft);
	if (input_sys.check_input_action(gstring::TurnDown))
		printf("%s\n", gstring::TurnDown);
	if (input_sys.check_input_action(gstring::TurnRight))
		printf("%s\n", gstring::TurnRight);
	if (input_sys.check_input_action(gstring::Forward))
		printf("%s\n", gstring::Forward);
	if (input_sys.check_input_action(gstring::Backward))
		printf("%s\n", gstring::Backward);
	//const mrbl::FramerateController& frc = mrbl::FramerateController::get();
	//printf("Current fps %f, max fps %f \n", mrbl::FramerateController::get().get_fps(), frc.get_max_fps());
}