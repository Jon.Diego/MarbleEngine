/*-------------------------------------------------
File: media_layer.cpp
Author: Jon Diego
Date: May 2018
Brief: Wrapper over the used media library to give it
	   RAII properties.
-------------------------------------------------*/
#pragma once
namespace mrbl
{
	/*--------------------------------------
	Wraps an underlying media library to give it 
	RAII properties.
	--------------------------------------*/
	class MediaLayer
	{
	public:
		//Inits the Library.
		MediaLayer();
		//Quits the library.
		~MediaLayer();
		//Not allowed.
		MediaLayer(const MediaLayer&) = delete;
	};
}