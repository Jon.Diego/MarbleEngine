/*-------------------------------------------------
File: media_layer.cpp
Author: Jon Diego
Date: May 2018
Brief: Wrapper over the used media library (SDL2.0 in this case) 
	   to give it RAII properties.
-------------------------------------------------*/
#include "media_layer.h"
#include "exception/exception.h"
#include "SDL/SDL.h"

namespace mrbl
{
	/*--------------------------------------
	brief: Constructor of the MediaLayer class.
		   Inits the SDL Library.
	--------------------------------------*/
	MediaLayer::MediaLayer()
	{
		if (SDL_Init(SDL_INIT_EVERYTHING) < 0)
			throw(Exception(std::string("Could not init SDL : ") + SDL_GetError()));
	}

	/*--------------------------------------
	brief: Destructor of the MediaLayer class.
		   Quits the SDL Library.
	--------------------------------------*/
	MediaLayer::~MediaLayer()
	{
		SDL_Quit();
	}
}