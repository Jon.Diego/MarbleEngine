/*-------------------------------------------------
File: window_manager.cpp
Author: Jon Diego
Date: May 2018
Brief: Part of the media layer. 
	   Handles the window creation and managing.
-------------------------------------------------*/
#pragma once
#include "containers/list.h"  //mrbl::list
#include "containers/array.h" //mrbl::array
#include "containers/string.h"//mrbl::string
#include <utility>	//std::pair

namespace mrbl
{
	//Number of windows in the main window array.
	constexpr unsigned main_windows_num = 4;

	/*--------------------------------------
	States a screen can be on. 
	--------------------------------------*/
	enum class ScreenTypes
	{
		FullScreen,
		Borderless_Window,
		Windowed
	};

	enum class VSyncType
	{
		NO_VSYNC,
		VSYNC,
		ADAPTATIVE_VSINC_IF_NOT_THEN_NO_VSYNC,
		ADAPTATIVE_VSINC_IF_NOT_THEN_VSYNC
	};

	/*--------------------------------------
	Logical representation of a desktop window. 
	--------------------------------------*/
	class Window
	{
	public:
		struct Handle;//Handle to the library's window type.
		struct Context;//Handle to the Graphics context.
		friend class WindowManager;
		//CODE:
		//Constructors.
		Window(const char* name, int width, int height, VSyncType v_sync_type);
		Window(const Window&) = delete;
		Window(Window&&) = delete;
		Window& operator=(const Window&) = delete;
		Window& operator=(Window&&) = delete;
		~Window();
		//Swaps the current buffer with the one in the screen.
		void swap_buffers();
		//Changes between screen types.
		void set_screen_type(ScreenTypes type);
		//Gets the size of the screen.
		std::pair<int, int> get_size();
		//Sets the size of the screen.
		void set_size(int w, int h);
		//Sets the postion of the screen.
		void set_pos(int x, int y);
		//Sets the GL viewport position and size within the screeen.
		void set_viewport(int x, int y, int w, int h);
		//Gets the index of the display the window is int in the desktop.
		int get_display_index();
		//Returns the name of the screen.
		const mrbl::string& get_name();
		//Gets the library representation of the window.
		Handle* get_handle();
		//Returns the library id of the window.
		unsigned get_id();
		//Makes the window ant it's context the current GL context.
		void make_current();
		//SetVSync
		VSyncType set_vsync(VSyncType desired_vsync);
		//Get Current VSync setting
		VSyncType get_vsync();

	private:
		//Data:
		Handle* m_handle;
		mrbl::string m_name;
		Context* m_context;
		VSyncType m_vsync;
	};

	/*--------------------------------------
	Stores and manages the lifetime of the windows.
	--------------------------------------*/
	class WindowManager
	{
	public:
		//CODE:
		//Constructor(Only one should be created per program)
		WindowManager();
		~WindowManager();

		//Access
		//Gets the Window manager object from anywhere in the program.
		static WindowManager& get();

		//Creates a new window, adding it to the window list.
		Window* create_window(const char * name, int width, int height, VSyncType vsync_type);
		//Returns the number of windows in the window list.
		size_t count_windows()const;
		//Destroys the first window in the list with a matching name.
		bool destroy_window(const char* name);
		//Destroys the window with the given library id.
		bool destroy_window(unsigned id);
		//Destroys the window in the list with the provided handle
		bool destroy_window(const Window::Handle* handle);
		//Returns the first window in the list with a matching name.
		Window* find_window(const char* name);
		//Returns the window with the given library id.
		Window* find_window(unsigned id);
		//Returns the window in the list with the provided handle
		Window* find_window(const Window::Handle* handle);
		//Returns the resolution of the display that has the provided index.
		std::pair<int,int> get_display_resolution(int index)const;

		//Main Window Array(an array to store pointers to windows on the list and be able to)
		//Access them without iterating the list. Must be set by the user.
		//Its max size is the main_windows_num const global and its methods are the following.

		//Gets the window ptr stored on the given index of the main window array.
		Window* get_main_window(size_t index);
		//Stores the given pointer on the given index of the main window array.
		void set_main_window(Window* window_ptr, size_t index);
		//Checks if a window pointer is in the main array and substitutes it with nullptr.
		bool clear_window_from_main_array(const Window* window_ptr);
	private:
		static WindowManager* s_manager;
		mrbl::list<Window> m_windows;
		mrbl::array<Window*, main_windows_num> m_main_windows;
	};
}