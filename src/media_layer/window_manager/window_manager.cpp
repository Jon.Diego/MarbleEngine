#include "window_manager.h"
#include "exception/exception.h"
#include "logger/logger.h"
#include "SDL/SDL.h"
#include "GL\gl_core_4_4.hpp"

namespace mrbl
{
	//WINDOW
	/*--------------------------------------
	brief: Constructs a window using the SDL media library.
	parameters:
		name: Name of the window to create.
		width: Width of the window.
		height: Height of the window.
	--------------------------------------*/
	Window::Window(const char* name,int width, int height, VSyncType v_sync_type): m_name(name)
	{
		// Create an application window with the following settings:
		m_handle = reinterpret_cast<Handle*>(SDL_CreateWindow(
			m_name.c_str(),							   // window title
			SDL_WINDOWPOS_UNDEFINED,           // initial x position
			SDL_WINDOWPOS_UNDEFINED,           // initial y position
			width,                             // width, in pixels
			height,                            // height, in pixels
			SDL_WINDOW_OPENGL                  // flags - see below
		));
		// Check that the window was successfully created
		if (m_handle == nullptr)
		{
			throw(Exception(mrbl::string("Could not create window: ") + SDL_GetError()));
		}
		//Create the OpenGL context.
		m_context = reinterpret_cast<Context*>(SDL_GL_CreateContext(reinterpret_cast<SDL_Window*>(m_handle)));
		if (m_context == nullptr)
		{
			throw(Exception(mrbl::string("Error In context creation: ") + SDL_GetError()));
		}
		if(SDL_GL_MakeCurrent(reinterpret_cast<SDL_Window*>(m_handle), reinterpret_cast<Context*>(m_context)) < 0)
		{
			throw(Exception(mrbl::string("Error assigning context to a window: ") + SDL_GetError()));
		}
		if (!gl::sys::LoadFunctions())
		{
			throw(Exception(mrbl::string("Could not load OpenGL functions: ") + SDL_GetError()));
		}
		set_vsync(v_sync_type);
	}

	/*--------------------------------------
	brief: Destructs the window using the SDL media library.
	--------------------------------------*/
	Window::~Window()
	{
		SDL_DestroyWindow(reinterpret_cast<SDL_Window*>(m_handle));
	}

	/*--------------------------------------
	brief: Swaps the current buffer with the one in the screen.
	--------------------------------------*/
	void Window::swap_buffers()
	{
		SDL_GL_SwapWindow(reinterpret_cast<SDL_Window*>(m_handle));
	}

	/*--------------------------------------
	brief: Changes between screen types.
	parameters:
		type: The type to be set.
	--------------------------------------*/
	void Window::set_screen_type(ScreenTypes type)
	{
		Uint32 sdl_type = [&type]()
		{
			switch (type)
			{
			case ScreenTypes::FullScreen:
				return SDL_WINDOW_FULLSCREEN;
				break;
			case ScreenTypes::Borderless_Window:
				return SDL_WINDOW_FULLSCREEN_DESKTOP;
				break;
			case ScreenTypes::Windowed:
				return static_cast<SDL_WindowFlags>(0);
				break;
			default:
				return static_cast<SDL_WindowFlags>(0);
				break;
			}
		}();

		if (SDL_SetWindowFullscreen(reinterpret_cast<SDL_Window*>(m_handle), sdl_type) < 0)
		{
			throw(Exception(mrbl::string("Error changing the window mode: ") + SDL_GetError()));
		}
	}
	
	/*--------------------------------------
	brief: Gets the size of the window.
	return:
		Pair with the width and height of the window.
	--------------------------------------*/
	std::pair<int, int> Window::get_size()
	{
		std::pair<int, int> pair;
		SDL_GetWindowSize(reinterpret_cast<SDL_Window*>(m_handle), &pair.first, &pair.second);
		return pair;
	}

	/*--------------------------------------
	brief: Sets the size of the screen.
	parameter:
		w: width of the screen.
		h: height of the screen.
	--------------------------------------*/
	void Window::set_size(int w, int h) 
	{
		SDL_SetWindowSize(reinterpret_cast<SDL_Window*>(m_handle), w, h);
	}

	/*--------------------------------------
	brief: Sets the postion of the screen.
	parameter:
		x: position of the screen in desktop x.
		y: position of the screen in desktop y.
	--------------------------------------*/
	void Window::set_pos(int x, int y)
	{
		SDL_SetWindowPosition(reinterpret_cast<SDL_Window*>(m_handle), x, y);
	}

	/*--------------------------------------
	brief: Sets the GL viewport position and size within the screeen.
	parameter:
		x: position of the viewport in window x.
		y: position of the viewport in window y.
		w: width of the viewport.
		h: height of the viewport.
	--------------------------------------*/
	void Window::set_viewport(int x, int y, int w, int h)
	{
		gl::Viewport(x, y, w, h);
		gl::MAX_UNIFORM_LOCATIONS;
		gl::MAX_VERTEX_UNIFORM_COMPONENTS;
		gl::MAX_FRAGMENT_UNIFORM_COMPONENTS;
	}

	/*--------------------------------------
	brief: Gets the index of the display the window is int in the desktop.
	return:
		The screen index.
	--------------------------------------*/
	int Window::get_display_index()
	{
		int result = SDL_GetWindowDisplayIndex(reinterpret_cast<SDL_Window*>(m_handle));
		if (result != -1)
			return result;
		else
			throw(Exception("Window has returned an incorrect display index."));
	}

	/*--------------------------------------
	brief: Returns the name of the screen.
	return:
		The screen name.
	--------------------------------------*/
	const mrbl::string& Window::get_name()
	{
		return m_name;
	}

	/*--------------------------------------
	brief: Gets the library representation of the window.
	return:
		The window handle.
	--------------------------------------*/
	Window::Handle* Window::get_handle()
	{
		return m_handle;
	}

	/*--------------------------------------
	brief: Returns the library id of the window.
	return:
		The window id
	--------------------------------------*/
	unsigned Window::get_id()
	{
		return SDL_GetWindowID(reinterpret_cast<SDL_Window*>(m_handle));
	}

	/*--------------------------------------
	brief: Makes the window ant it's context the current GL context.
	--------------------------------------*/
	void Window::make_current()
	{
		SDL_GL_MakeCurrent(reinterpret_cast<SDL_Window*>(m_handle), reinterpret_cast<Context*>(m_context));
	}

	/*-------------------------------------
	brief: Tells SDL to set a certain level of VSync
	return: 
		Returns the level of VSync that was finally set.
	---------------------------------------*/
	VSyncType Window::set_vsync(VSyncType desired_vsync)
	{
		int sdl_primary_option;
		int sdl_secondary_option;
		switch (desired_vsync)
		{
		case mrbl::VSyncType::NO_VSYNC:
			sdl_primary_option = 0;
			sdl_secondary_option = 0;
			break;
		case mrbl::VSyncType::VSYNC:
			sdl_primary_option = 1;
			sdl_secondary_option = 0;
			break;
		case mrbl::VSyncType::ADAPTATIVE_VSINC_IF_NOT_THEN_NO_VSYNC:
			sdl_primary_option = -1;
			sdl_secondary_option = 0;
			break;
		case mrbl::VSyncType::ADAPTATIVE_VSINC_IF_NOT_THEN_VSYNC:
			sdl_primary_option = -1;
			sdl_secondary_option = 1;
			break;
		default:
			mrbl::Logger::get().write("Unkown vertical sync option");
			sdl_primary_option = 0;
			sdl_secondary_option = 0;
			break;
		}

		if (SDL_GL_SetSwapInterval(sdl_primary_option))
		{
			mrbl::Logger::get().write((mrbl::string("Desired VSync option not supported") + SDL_GetError()).c_str());
			if (SDL_GL_SetSwapInterval(sdl_secondary_option))
			{
				mrbl::Logger::get().write((mrbl::string("FallBack VSync option not supported") + SDL_GetError()).c_str());
				if (SDL_GL_SetSwapInterval(0))
				{
					throw(Exception(mrbl::string("No VSync option supported. SDL error:") + SDL_GetError()));
				}
			}
			return sdl_secondary_option == 1 ? mrbl::VSyncType::VSYNC : mrbl::VSyncType::NO_VSYNC;
		}
		return desired_vsync;
	}

	/*-------------------------------------
	brief: returns the current level of VSync set by SDL.
	return:
		The level of VSync.
	--------------------------------------*/
	VSyncType Window::get_vsync()
	{
		return m_vsync;
	}

	//WINDOW MANAGER
	WindowManager* WindowManager::s_manager = nullptr;

	//Constructor. Will throw if two windows managers are created.
	WindowManager::WindowManager(): m_main_windows{nullptr}
	{
		if (s_manager != nullptr)
			throw(Exception("Tried to instantiate a second windows manager."));
		s_manager = this;
		SDL_GL_SetAttribute(SDL_GL_CONTEXT_PROFILE_MASK, SDL_GL_CONTEXT_PROFILE_CORE);
		SDL_GL_SetAttribute(SDL_GL_CONTEXT_MAJOR_VERSION, 4);
		SDL_GL_SetAttribute(SDL_GL_CONTEXT_MINOR_VERSION, 4);
	}

	WindowManager::~WindowManager()
	{
		if (s_manager != this)
			Logger::get().write("Window Manager pointer was changed before deletion");
		else
			s_manager = nullptr;
	}

	//Access
	/*--------------------------------------
	brief: Gets the Window manager object from anywhere in the program.
	return:
		   Reference to the window manager object.
	--------------------------------------*/
	WindowManager& WindowManager::get()
	{
		return *s_manager;
	}

	/*--------------------------------------
	brief: Creates a new window, adding it to the window list.
	parameters:
		name: Name of the new window.
		width: width of the new window.
		height: height of the new window.
	return:
			ptr to the created window. nullptr if it could not be created.
	--------------------------------------*/
	Window* WindowManager::create_window(const char * name, int width, int height, VSyncType vsync_type)
	{
		try
		{
			m_windows.emplace_back(name, width, height, vsync_type);
			return &m_windows.back();
		}
		catch (const Exception&)
		{
			return nullptr;
		}
	}

	/*--------------------------------------
	brief: Returns the number of windows in the window list.
	return:
			The number of windows in the window list.
	--------------------------------------*/
	size_t WindowManager::count_windows()const
	{
		return m_windows.size();
	}

	/*--------------------------------------
	brief: Destroys the first window in the list with a matching name.
	parameters:
		name: The name to look for.
	return:
		true if a window was destroyed, false if not.
	--------------------------------------*/
	bool WindowManager::destroy_window(const char* name)
	{
		mrbl::list<Window>::iterator it = m_windows.begin();
		for (; it != m_windows.end(); ++it)
		{
			if (it->m_name == name)
			{
				m_windows.erase(it);
				return true;
			}
		}
		return false;
	}

	/*--------------------------------------
	brief: Destroys the window in the list with the provided library id.
	parameters:
		index: The library id of the window to destroy.
	return:
		true if a window was destroyed, false if not.
	--------------------------------------*/
	bool WindowManager::destroy_window(unsigned id)
	{
		for (mrbl::list<Window>::iterator it = m_windows.begin();
			it != m_windows.end(); ++it)
		{
			if (id == it->get_id())
			{
				m_windows.erase(it);
				return true;
			}
		}
		return false;
	}

	/*--------------------------------------
	brief: Destroys the window in the list with the provided handle.
	parameters:
		handle: The handle of the window to destroy.
	return:
		true if a window was destroyed, false if not.
	--------------------------------------*/
	bool WindowManager::destroy_window(const Window::Handle* handle)
	{
		for (mrbl::list<Window>::iterator it = m_windows.begin();
			it != m_windows.end(); ++it)
		{
			if (it->m_handle == handle)
			{
				m_windows.erase(it);
				return true;
			}
		}
		return false;
	}

	/*--------------------------------------
	brief: Returns the first window in the list with a matching name.
	parameters:
		name: The name to look for.
	return:
		ptr to the found window, nullptr if none was found.
	--------------------------------------*/
	Window* WindowManager::find_window(const char* name)
	{
		for (Window& window : m_windows)
		{
			if (window.m_name == name)
				return &window;
		}
		return nullptr;
	}

	/*--------------------------------------
	brief: Returns the window with the given library id.
	parameters:
		id: The library id of the window to find.
	return:
		ptr to the found window, nullptr if none was found.
	--------------------------------------*/
	Window* WindowManager::find_window(unsigned id)
	{
		
		for (mrbl::list<Window>::iterator it = m_windows.begin();
			it != m_windows.end(); ++it)
		{
			if (id == it->get_id())
			{
				return &(*it);
			}
		}
		return nullptr;
	}

	/*--------------------------------------
	brief: Returns the window in the list with the provided handle.
	parameters:
		handle: The handle of the window to find.
	return:
		ptr to the found window, nullptr if none was found.
	--------------------------------------*/
	Window* WindowManager::find_window(const Window::Handle* handle)
	{
		for (mrbl::list<Window>::iterator it = m_windows.begin();
			it != m_windows.end(); ++it)
		{
			if (it->m_handle == handle)
			{
				return &(*it);
			}
		}
		return nullptr;
	}

	/*--------------------------------------
	brief: Returns the window on the given index of the list.
	parameters:
	name: The index of the list to return.
	return:
	ptr to the found window, nullptr if none was found.
	--------------------------------------*/
	std::pair<int, int> WindowManager::get_display_resolution(int index)const
	{
		SDL_DisplayMode mode;
		std::pair<int, int> result;
		SDL_GetDesktopDisplayMode(index , &mode);
		result.first = mode.w;
		result.second = mode.h;
		return result;
	}

	/*--------------------------------------
	brief: Gets the window ptr stored on the given index of the main window array.
	parameters:
		index: The index to look at.
	return:
		Pointer to that location in the array.
	--------------------------------------*/
	Window* WindowManager::get_main_window(size_t index)
	{
		return m_main_windows[index];
	}


	/*--------------------------------------
	brief: Stores the given pointer on the given index of the main window array.
	parameters:
		window_ptr: window pointer to store.
		index: The of the array to store the pointer in.
	--------------------------------------*/
	void WindowManager::set_main_window(Window* window_ptr, size_t index)
	{
		m_main_windows[index] = window_ptr;
	}

	//Checks if a window pointer is in the main array and substitutes it with nullptr.
	bool WindowManager::clear_window_from_main_array(const Window* window_ptr)
	{
		bool found = false;
		for (unsigned i = 0; i < main_windows_num; ++i)
		{
			if (get_main_window(i) == window_ptr)
			{
				found = true;
				set_main_window(nullptr,i);
			}
		}
		return found;
	}
}