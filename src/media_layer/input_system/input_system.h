/*-------------------------------------------------
File: input_system.h
Author: Jon Diego
Date: May 2018
Brief: Part of the media layer.
	   This file holds the input system that handles
	   the input provided by the media layer library.
-------------------------------------------------*/
#pragma once
#include "button_info.h" //ButtonStates, MouseButtons, Keys
#include "containers/string.h"	//mrbl::string
#include "containers/unordered_map.h"	//mrbl::unordered_map
#include "containers/vector.h"	//mrbl::vector
#include <utility>	//std::pair	



namespace mrbl
{
	const unsigned char key_array_size = static_cast<unsigned char>(Keys::Last);
	const unsigned char mouse_array_size = static_cast<unsigned char>(MouseButtons::Last);

	/*--------------------------------------
	Handles the input provided by the media layer library
	--------------------------------------*/
	class InputSystem
	{
	public:
		struct Event;
		//Used to abstract the input from the phisical keys.
		struct InputAction
		{
			ButtonStates m_state;
			DeviceTypes m_type;
			union
			{
				Keys m_key;
				MouseButtons m_mouse_button;
			};
		};
		//CODE
		//Constructor
		InputSystem();
		~InputSystem();

		//Access
		static InputSystem& get();

		//Operational.
		//Reads all the pending events to be processed by the existing windows.
		bool pool_events();

		//Returns if the key is in certain status. Considers that releases are also nones and
		//that triggers are also presseds.
		bool is_key_in_state(const Keys key, const ButtonStates state)const;
		//Returns if the mousebutton is in certain status. Considers that releases are also nones and
		//that triggers are also presseds.
		bool is_mousebutton_in_state(const MouseButtons button,const ButtonStates state)const;
		//Returns the current status of a keyboard key. Does not do any additional logic.
		ButtonStates get_key_status(const Keys key)const;
		//Returns the current status of a mouse button. Does not do any additional logic.
		ButtonStates get_mousebutton_status(const MouseButtons button)const;
		
		//Returns the current position of the mouse.
		std::pair<unsigned, unsigned> get_mouse_pos()const;

		//Adds an InputAction to the action map.
		void add_input_action(const char* action_name, InputAction action);
		//Checks if an input Action has taken place.
		bool check_input_action(const char* action_name)const;

	private:
		//CODE
		//Clears all the info arrays.
		void clear_last_frame_info();
		//Clears a given info array, making triggers presseds and released nones.
		void clear_input_array(ButtonStates* array, unsigned char array_size);
		//Changes a value in one of the key/button arrays. Does not change it if the current value already contains the new value.
		void change_value(ButtonStates* array,unsigned char array_size, unsigned int button, ButtonStates state);
		//Helper function to handle window events.
		void handle_window_event(const Event& event)const;
		
		//DATA
		static InputSystem* s_system;
		ButtonStates m_keyarray [key_array_size];
		ButtonStates m_mousearray [mouse_array_size];
		unsigned int mousepos_[2];
		mrbl::unordered_map<mrbl::string, mrbl::vector<InputAction>> m_action_map;
	};

}