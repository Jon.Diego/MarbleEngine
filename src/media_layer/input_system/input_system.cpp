/*-------------------------------------------------
File: input_system.cpp
Author: Jon Diego
Date: May 2018
Brief: Part of the media layer.
	   This file holds the input system that handles
	   the input provided by the media layer library.
-------------------------------------------------*/
#include "input_system.h"
#include "exception/exception.h"
#include "logger/logger.h"
#include "media_layer/window_manager/window_manager.h"
#include "SDL/SDL.h"
//#include <cstdio>

namespace mrbl
{

	InputSystem* InputSystem::s_system = nullptr;

	/*--------------------------------------
	brief: Check if one button state is contained within the other.
	parameters:
		contained: The state we will check if is contained.
		state: The state we will check if it contains the other.
	return:
		True if the constained state is trully contained within the container state.
	--------------------------------------*/
	static bool is_state_contained(ButtonStates contained, ButtonStates container)
	{
		switch (container)
		{
		case ButtonStates::None:
			return !(static_cast<unsigned char>(contained) & ~static_cast<unsigned char>(ButtonStates::Released));
		case ButtonStates::Pressed:
			return (static_cast<unsigned char>(contained) & (static_cast<unsigned char>(ButtonStates::Triggered) + static_cast<unsigned char>(ButtonStates::Pressed)));
		default:
			return contained == container;
			break;
		}
	}

	/*--------------------------------------
	brief: Constructor of the InputSystem class. May throw if another Input system already exists.
	--------------------------------------*/
	InputSystem::InputSystem() :m_keyarray{ ButtonStates::None }, m_mousearray{ ButtonStates::None }
	{
		if (s_system != nullptr)
			throw(Exception("Tried to instantiate a second Input System."));
		s_system = this;
	}

	/*--------------------------------------
	brief: Destructor of the InputSysyem class.
	--------------------------------------*/
	InputSystem::~InputSystem()
	{
		if (s_system != this)
			Logger::get().write("Input System pointer was changed before deletion");
		else
			s_system = nullptr;
	}

	/*--------------------------------------
	brief: Get the instance of the system.
	return:
		Reference to the input system.
	--------------------------------------*/
	InputSystem& InputSystem::get()
	{
		return *s_system;
	}

	/*--------------------------------------
	brief: Reads all the pending events to 
		   be processed by the existing windows.
	return:
		   false if a quit event was received, 
		   true otherwise.
	--------------------------------------*/
	bool InputSystem::pool_events()
	{
		SDL_Event event;
		clear_last_frame_info();
		while (SDL_PollEvent(&event))
		{
			switch (event.type)
			{
			case SDL_KEYDOWN:
				printf("Key_Down %i\n", event.button.button);
				change_value(m_keyarray, key_array_size, event.button.button, ButtonStates::Triggered);
				break;
			case SDL_KEYUP:
				change_value(m_keyarray, key_array_size, event.button.button, ButtonStates::Released);
				break;
			case SDL_MOUSEMOTION:
				mousepos_[0] = event.motion.x;
				mousepos_[1] = event.motion.y;
				break;
			case SDL_MOUSEBUTTONDOWN:
				change_value(m_mousearray, mouse_array_size, event.button.button - 1, ButtonStates::Triggered);
				break;
			case SDL_MOUSEBUTTONUP:
				change_value(m_mousearray, mouse_array_size,event.button.button - 1, ButtonStates::Released);
				break;
			case SDL_WINDOWEVENT:
				handle_window_event(reinterpret_cast<InputSystem::Event&>(event));
				break;
			case SDL_APP_TERMINATING:
				Logger::get().write_and_flush("System required termination");
			case SDL_QUIT:
				//Close The game.
				return false;
				break;
			default:
				break;
			}
		}
		return true;
	}

	/*--------------------------------------
	brief: Returns if the key is in certain status. Considers that releases are also nones and
		   that triggers are also presseds.
	parameters:
		key: The key to check.
		state: The state to check against.
	return:
		True if the state of the key is contained in the provided state.
	--------------------------------------*/
	bool InputSystem::is_key_in_state(const Keys key, const ButtonStates state)const
	{
		return is_state_contained(get_key_status(key),state);
	}


	/*--------------------------------------
	brief: Returns if the mousebutton is in certain status. Considers that releases are also nones and
		   that triggers are also presseds.
	parameters:
		button: The mousebutton to check.
		state: The state to check against.
	return:
		True if the state of the button is contained in the provided state.
	--------------------------------------*/
	bool InputSystem::is_mousebutton_in_state(const MouseButtons button, const ButtonStates state)const
	{
		return is_state_contained(get_mousebutton_status(button), state);
	}

	/*--------------------------------------
	brief: Returns the current status of a keyboard key.Does not do any additional logic.
	parameters:
		key: the key index whose status we are looking for.
	return:
		The state of the keyboard key.
	--------------------------------------*/
	ButtonStates InputSystem::get_key_status(const Keys key)const
	{
		return m_keyarray[static_cast<unsigned int>(key)];
	}

	/*--------------------------------------
	brief: Returns the current status of a mouse button.Does not do any additional logic.
	parameters:
		button: the button whose status we are looking for. 
	return:
		The state of the mouse button.
	--------------------------------------*/
	ButtonStates InputSystem::get_mousebutton_status(const MouseButtons button)const
	{
		return m_mousearray[static_cast<unsigned int>(button)];
	}

	/*--------------------------------------
	brief: Returns the current position of the mouse.
	return:
		The x and y coordinates of the mouse.
	--------------------------------------*/
	std::pair<unsigned, unsigned> InputSystem::get_mouse_pos()const
	{
		std::pair<unsigned, unsigned> result;
		result.first = mousepos_[0];
		result.second = mousepos_[1];
		return result;
	}

	/*--------------------------------------
	brief: Adds an InputAction to the action map.
	parameters:
		action_name: The name of the action.
		action: The action instance.
	--------------------------------------*/
	void InputSystem::add_input_action(const char* action_name, InputAction action)
	{
		m_action_map[action_name].push_back(std::move(action));
	}

	/*--------------------------------------
	brief: Checks if an input Action has taken place.
	parameters:
		action_name: The name of the action.
	return: 
		True if the action has happened this frame, false otherwise.
	--------------------------------------*/
	bool InputSystem::check_input_action(const char* action_name)const
	{
		decltype(m_action_map)::const_iterator it = m_action_map.find(action_name);
		if (it != m_action_map.end())
		{
			for (const InputAction& action : it->second)
			{
				switch (action.m_type)
				{
				case DeviceTypes::KeyBoard:
					if(is_key_in_state(action.m_key, action.m_state))
						return true;
					break;
				case DeviceTypes::Mouse:
					if(is_mousebutton_in_state(action.m_mouse_button, action.m_state))
						return true;
					break;
				default:
					break;
				}
			}
		}
		return false;
	}

	/*--------------------------------------
	brief: Clears all the info arrays.
	--------------------------------------*/
	void InputSystem::clear_last_frame_info()
	{
		clear_input_array(m_keyarray,key_array_size);
		clear_input_array(m_mousearray,mouse_array_size);
	}

	/*--------------------------------------
	brief: Clears a given info array, 
		   making triggers presseds and released nones.
	parameters:
		array: The array to clear.
		array_size: the size of the array.
	--------------------------------------*/
	void InputSystem::clear_input_array(ButtonStates* array, unsigned char array_size)
	{
		for (unsigned int i = 0; i < array_size; ++i)
		{
			const unsigned char pressed = static_cast<unsigned char>(ButtonStates::Pressed);
			unsigned char temp = static_cast<unsigned char>(array[i]);
			unsigned char temp2 = (temp << 1) & pressed;
			temp &=pressed;
			array[i] = static_cast<ButtonStates>(temp | temp2);
		}
	}

	/*--------------------------------------
	brief: Changes a value in one of the key/button arrays.
		   Does not change it if it already contains the new value.
	parameters:
		array: The array to change a value on.
		array_size: the size of the array.
		button: the index of the button to change.
		state: the new state to set the button on.
	--------------------------------------*/
	void InputSystem::change_value(ButtonStates* array, unsigned char array_size, unsigned int button, ButtonStates state)
	{
		if (button >= array_size)
		{
			Logger::get().write("Inputed Button is bigger than the corresponding array size.");
			return;
		}
		//Safety check as SDL seems to give new trigger events after long presses.
		if(!is_state_contained(state,array[button]))
			array[button] = state;
	}

	/*--------------------------------------
	brief: Helper function to handle window events.
	parameters:
		casted_event: The array to change a value on.
	--------------------------------------*/
	void InputSystem::handle_window_event(const Event& casted_event)const
	{
		const SDL_Event& event = reinterpret_cast<const SDL_Event&>(casted_event);
		switch (event.window.event)
		{
		case SDL_WINDOWEVENT_CLOSE:
		{
			WindowManager& w_man = WindowManager::get();
			Window* win_ptr = w_man.find_window(event.window.windowID);
			if (win_ptr == nullptr)
				break;
			w_man.clear_window_from_main_array(win_ptr);
			w_man.destroy_window(win_ptr->get_handle());
		}
			break;
		default:
			break;
		}
	}
}