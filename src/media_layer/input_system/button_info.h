#pragma once


namespace mrbl
{
	//State of the buttons.
	enum class ButtonStates : unsigned char
	{
		None = 0,
		Triggered = 1,
		Pressed = 2,
		Released = 4
	};

	//Input Devices
	enum class DeviceTypes : unsigned char
	{
		KeyBoard = 0,
		Mouse = 1
	};

	//Mouse buttons.
	enum class MouseButtons : unsigned char
	{
		Left = 0,
		Middle = 1,
		Right = 2,
		Last = 3
	};

	//Keyboard keys.
	enum class Keys : unsigned char
	{
		//numbers
		Key_1 = 30,
		Key_2 = 31,
		Key_3 = 32,
		Key_4 = 33,
		Key_5 = 34,
		Key_6 = 35,
		Key_7 = 36,
		Key_8 = 37,
		Key_9 = 38,
		Key_0 = 39,
		//Letters
		Key_A = 4,
		Key_B = 5,
		Key_C = 6,
		Key_D = 7,
		Key_E = 8,
		Key_F = 9,
		Key_G = 10,
		Key_H = 11,
		Key_I = 12,
		Key_J = 13,
		Key_K = 14,
		Key_L = 15,
		Key_M = 16,
		Key_N = 17,
		Key_O = 18,
		Key_P = 19,
		Key_Q = 20,
		Key_R = 21,
		Key_S = 22,
		Key_T = 23,
		Key_U = 24,
		Key_V = 25,
		Key_W = 26,
		Key_X = 27,
		Key_Y = 28,
		Key_Z = 29,
		Key_� = 51,
		//Other
		Key_ENTER_L = 40,
		Key_ENTER_R = 88,
		Key_CONTROL_L = 224,
		Key_CONTROL_R = 228,
		Key_MAYUS_L = 225,
		Key_MAYUS_R = 229,
		Key_SPACE = 44,
		Key_ESC = 41,
		Key_BACKSPACE = 42,
		Key_DEL = 76,
		Key_TAB = 43,
		Key_BLOQ_MAYUS = 57,
		/***************/
		Key_ALT_L = 226,
		//Key_ALT_Gr
		Key_ALT_R = 224,
		Key_Gr = 230,
		/***************/
		//Directional Keys
		Key_RIGHT = 79,
		Key_LEFT = 80,
		Key_DOWN = 81,
		Key_UP = 82,
		/***************/
		//Function keys
		Key_F1 = 58,
		Key_F2 = 59,
		Key_F3 = 60,
		Key_F4 = 61,
		Key_F5 = 62,
		Key_F6 = 63,
		Key_F7 = 64,
		Key_F8 = 65,
		Key_F9 = 66,
		Key_F10 = 67,
		Key_F11 = 68,
		Key_F12 = 69,

		Last = 255,
	};
}