/*-------------------------------------------------
File: exception.cpp
Author: Jon Diego
Date: May 2018
Brief: Contains the custom exceptions that will be
	used by the marble engine.
-------------------------------------------------*/
#include "exception.h"


namespace mrbl
{
	/*--------------------------------------
	brief: Constructor of the exception class.
	parameters:
		message: message to be stored int he exception.
	--------------------------------------*/
	Exception::Exception(mrbl::string message):m_message(std::move(message))
	{}

	/*--------------------------------------
	brief: Override of the what virtual function of the std::exception
		   base class.
	return:
		The message of the exception.
	--------------------------------------*/
	const char * Exception::what() const
	{
		return m_message.c_str();
	}

}