/*-------------------------------------------------
File: exception.h
Author: Jon Diego
Date: May 2018
Brief: Contains the custom exceptions that will be
       used by the marble engine.
-------------------------------------------------*/
#pragma once
#include "containers/string.h"	//mrbl::string
#include <exception>//std::exception

namespace mrbl
{
	/*--------------------------------------
	Custom exception class to be used by the 
	marble engine.
	--------------------------------------*/
	struct Exception : public std::exception
	{
		//Constructor of the exception class.
		Exception(mrbl::string message);
		//Override of the what virtual function of the std::exception base class.
		const char * what() const override;
	private:
		mrbl::string m_message;
	};
}