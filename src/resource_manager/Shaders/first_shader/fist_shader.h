/*-------------------------------------------------
File: first_shader.h
Author: Jon Diego
Date: August 2018
Brief: A dummy shader that takes an vbo and draws it.
	   Used as a way to test the pipeline.
-------------------------------------------------*/
#pragma once

#include "resource_manager/shaders/base_shader/shader.h"


namespace mrbl
{
	class FirstShader : public Shader
	{
		RTTI_DECLARATION;
	public:
		FirstShader(mrbl::string name,
			mrbl::string vertexpath,
			mrbl::string tesselationcontrolpath,
			mrbl::string tesselationevaluationpath,
			mrbl::string geometrypath,
			mrbl::string fragmentpath,
			mrbl::string computepath);
		
		void get_uniforms() override;
	private:
		void inner_render(std::any data) override;

	};
}