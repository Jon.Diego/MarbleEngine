/*-------------------------------------------------
File: first_shader.h
Author: Jon Diego
Date: August 2018
Brief: A dummy shader that takes an vbo and draws it.
	   Used as a way to test the pipeline.
-------------------------------------------------*/
#include "fist_shader.h"
#include "GL/gl_core_4_4.hpp"//Opengl types and functions.
#include "resource_manager/meshes/mesh.h"
#include "serialization/printable_enum_class/printable_enum_class.h"
#include "transform/transform.h"

#include <GLM/gtc/type_ptr.hpp>//glm::value_ptr



namespace mrbl
{
	PRINTABLE_ENUM_CLASS(Uniforms, OBJ_MATRIX, COUNT);

	RTTI_IMPLEMENTATION(FirstShader, &Shader::get_rtti_static());

	/*--------------------------------------
	brief: Shader Constructor
	params:
		name: The name of the shader.
		vertexpath: Path of the vertex shader file.
		tesselationcontrolpath: Path of the tesselation control shader file.
		tesselationevaluationpath: Path of the tesselation evaluation shader file.
		geometrypath: Path of the geometry shader file.
		fragmentpath: Path of the fragment shader file.
		computepath: Path of the compute shader file.
	--------------------------------------*/
	FirstShader::FirstShader(mrbl::string name,
		mrbl::string vertexpath,
		mrbl::string tesselationcontrolpath,
		mrbl::string tesselationevaluationpath,
		mrbl::string geometrypath,
		mrbl::string fragmentpath,
		mrbl::string computepath)
		: Shader(std::move(name), std::move(vertexpath),std::move(tesselationcontrolpath),std::move(tesselationevaluationpath),
			std::move(geometrypath),std::move(fragmentpath),std::move(computepath))
	{}


	/*--------------------------------------
	brief: Stores the uniform's indexes in an array.
		   As this kind of shaders has no uniforms this is a dummy function.
	--------------------------------------*/
	void FirstShader::get_uniforms()
	{
		m_uniformvector.clear();
		m_uniformvector.reserve(static_cast<size_t>(Uniforms::COUNT));
		for (size_t i = 0; i < static_cast<size_t>(Uniforms::COUNT); ++i)
		{
			m_uniformvector.push_back(gl::GetUniformLocation(m_shaderprogram, enum_to_string<Uniforms>(static_cast<Uniforms>(i)).c_str()));
		}
	}

	/*--------------------------------------
	brief: Shader render's function.
	params:
	   data: In this case data is simply the index of the VAO to draw.
	--------------------------------------*/
	void FirstShader::inner_render(std::any data)
	{
		std::pair<Mesh*, Transform*> my_pair = std::any_cast<std::pair<Mesh*,Transform*>>(data);
		Mesh* my_mesh = my_pair.first;
		Transform* my_transform = my_pair.second;
		my_mesh->bind();
		gl::UniformMatrix4fv(m_uniformvector[static_cast<int>(Uniforms::OBJ_MATRIX)],1,false, glm::value_ptr(my_transform->get_local_matrix()));
		gl::DrawArrays(gl::TRIANGLES, 0, my_mesh->get_vertex_num());
	}
}