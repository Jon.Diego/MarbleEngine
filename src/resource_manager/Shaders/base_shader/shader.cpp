/*-------------------------------------------------
File: shader.cpp
Author: Jon Diego
Date: August 2018
Brief: Base of any Shader. Allows us to work with opengl
	   shaders easily
-------------------------------------------------*/
#include "shader.h"
#include <cmath>
#include "GL/gl_core_4_4.hpp"//Opengl types and functions.
#include <fstream>//fstream
#include <sstream>//stringstream.
#include "stdio.h"//printf
#include "exception/exception.h"
#include "logger/logger.h"


namespace mrbl
{
	RTTI_IMPLEMENTATION(Shader, &Resource::get_rtti_static());

	//Last Shader optimization.
	const Shader* Shader::s_last_shader = nullptr;
	namespace
	{
		/*--------------------------------------
		brief: Loads a shader file into a string.
		params:
			filepath: filepath where the shader is located.
		return:
			The string with the shader loaded in it.
        --------------------------------------*/
		mrbl::string load_shader_file(const mrbl::string& filepath)
		{
			std::fstream vs(filepath.data(), std::fstream::in);
			if (!vs.is_open())
			{
				mrbl::string error_msg("Error, could not open " + filepath);
				Logger::get().write(error_msg.c_str());
				return "";
			}
			else
			{
				std::stringstream vShaderStream;
				vShaderStream << vs.rdbuf();
				vs.close();
				return vShaderStream.str();
			}
		}
		/*--------------------------------------
		brief: Compiles a part of a shader program.
		params:
			shader_source: The shader in text form.
			new_shader: The handler of the newly created shader will be placed here.
			shader_type: The kind of shader this is (vertex, fragment...etc)
		return:
			True if the shader was succesfully compiled false otherwise.
		--------------------------------------*/
		bool compile_partial_shader(const GLchar* shader_source, GLuint& new_shader, GLuint shader_type)
		{
			new_shader = gl::CreateShader(shader_type);
			//compile vertex shader
			gl::ShaderSource(new_shader, 1, &shader_source, NULL);
			gl::CompileShader(new_shader);
			//check if the compilation was succesful
			GLint success;
			GLchar infoLog[1024];
			gl::GetShaderiv(new_shader, gl::COMPILE_STATUS, &success);
			if (!success)
			{
				mrbl::string error_msg;
				Logger::get().write(error_msg.c_str());
				gl::GetShaderInfoLog(new_shader, 1024, NULL, infoLog);
				switch (shader_type)
				{
				case gl::VERTEX_SHADER:
					error_msg += "ERROR::VERTEX_SHADER::COMPILATION_FAILED\n ";
					error_msg += (char*)infoLog;
					Logger::get().write(error_msg.c_str());
					break;
				case gl::TESS_CONTROL_SHADER:
					error_msg += "ERROR::TESS_CONTROL_SHADER::COMPILATION_FAILED\n ";
					error_msg += (char*)infoLog;
					Logger::get().write(error_msg.c_str());
					break;
				case gl::TESS_EVALUATION_SHADER:
					error_msg += "ERROR::TESS_EVALUATION_SHADER::COMPILATION_FAILED\n ";
					error_msg += (char*)infoLog;
					Logger::get().write(error_msg.c_str());
					break;
				case gl::GEOMETRY_SHADER:
					error_msg += "ERROR::GEOMETRY_SHADER::COMPILATION_FAILED\n ";
					error_msg += (char*)infoLog;
					Logger::get().write(error_msg.c_str());
					break;
				case gl::FRAGMENT_SHADER:
					error_msg += "ERROR::FRAGMENT_SHADER::COMPILATION_FAILED\n ";
					error_msg += (char*)infoLog;
					Logger::get().write(error_msg.c_str());
					break;
				case gl::COMPUTE_SHADER:
					error_msg += "ERROR::COMPUTE_SHADER::COMPILATION_FAILED\n ";
					error_msg += (char*)infoLog;
					Logger::get().write(error_msg.c_str());
					break;
				default:
					error_msg += "ERROR::UNKNOWN_SHADER_STAGE::COMPILATION_FAILED\n ";
					error_msg += (char*)infoLog;
					Logger::get().write(error_msg.c_str());
					break;
				}
				return false;
			}
			return true;
		}
	}
	/*--------------------------------------
	brief: Shader Constructor
	params:
		name: The name of the shader.
		vertexpath: Path of the vertex shader file.
		tesselationcontrolpath: Path of the tesselation control shader file.
		tesselationevaluationpath: Path of the tesselation evaluation shader file.
		geometrypath: Path of the geometry shader file.
		fragmentpath: Path of the fragment shader file.
		computepath: Path of the compute shader file.
	--------------------------------------*/
	Shader::Shader(mrbl::string name,
		mrbl::string vertexpath,
		mrbl::string tesselationcontrolpath,
		mrbl::string tesselationevaluationpath,
		mrbl::string geometrypath,
		mrbl::string fragmentpath,
		mrbl::string computepath) :Resource(std::move(name)), m_shaderprogram(gl::NONE), m_vertexpath(std::move(vertexpath)), m_tesselationcontrolpath(std::move(tesselationcontrolpath)), m_tesselationevaluationpath(std::move(tesselationevaluationpath)), m_geometrypath(std::move(geometrypath)), m_fragmentpath(std::move(fragmentpath)), m_computepath(std::move(computepath))
	{
		compile_shader();
	}

	/*--------------------------------------
	brief: Destructor
	--------------------------------------*/
	Shader::~Shader()
	{
		gl::DeleteProgram(m_shaderprogram);
	}

	/*--------------------------------------
	brief: Applies the Last shader optimization and after calls the 
	virtual render function.
	params:
		data: the data to pass to the virtual render function.
	--------------------------------------*/
	void Shader::render(std::any data)
	{
		if (s_last_shader != this)
		{
			gl::UseProgram(m_shaderprogram);
			s_last_shader = this;
		}
		this->inner_render(data);
	}

	/*--------------------------------------
	brief: Returns a uniform location for this shaderprogram.
	params:
		index: The index of the uniform we are looking for.
	return:
		The indentifier of the asked uniform in opengl.
	--------------------------------------*/
	int Shader::get_uniform(unsigned int index)const
	{
		return m_uniformvector[index];
	}

	/*--------------------------------------
	brief: Compiles an opengl shader program.
	--------------------------------------*/
	void Shader::compile_shader()
	{
		//Create a Shader program
		m_shaderprogram = gl::CreateProgram();
		GLuint vertexShader = 0;
		GLuint tesselationControlShader = 0;
		GLuint tesselationEvaluationShader = 0;
		GLuint geometryShader = 0;
		GLuint fragmentshader = 0;
		GLuint computepath = 0;

		//Attach the shaders

		//VertexShader
		if (m_vertexpath != (""))
		{
			if (compile_partial_shader(load_shader_file(m_vertexpath).c_str(), vertexShader, gl::VERTEX_SHADER))
			{
				gl::AttachShader(m_shaderprogram, vertexShader);
			}
			else
			{
				Logger::get().write(("ERROR::STAGE_AVOIDED::" + get_name()).c_str());
			}
		}
		//TesselationShader
		if (m_tesselationcontrolpath != (""))
		{
			if (compile_partial_shader(load_shader_file(m_tesselationcontrolpath).c_str(), tesselationControlShader, gl::TESS_CONTROL_SHADER))
			{
				gl::AttachShader(m_shaderprogram, tesselationControlShader);
			}
			else
			{
				Logger::get().write(("ERROR::STAGE_AVOIDED::" + get_name()).c_str());
			}
		}
		if (m_tesselationevaluationpath != (""))
		{
			if (compile_partial_shader(load_shader_file(m_tesselationevaluationpath).c_str(), tesselationEvaluationShader, gl::TESS_EVALUATION_SHADER))
			{
				gl::AttachShader(m_shaderprogram, tesselationEvaluationShader);
			}
			else
			{
				Logger::get().write(("ERROR::STAGE_AVOIDED::" + get_name()).c_str());
			}
		}
		//GeometryShader
		if (m_geometrypath != "")
		{
			if (compile_partial_shader(load_shader_file(m_geometrypath).c_str(), geometryShader, gl::GEOMETRY_SHADER))
			{
				gl::AttachShader(m_shaderprogram, geometryShader);
			}
			else
			{
				Logger::get().write(("ERROR::STAGE_AVOIDED::" + get_name()).c_str());
			}
		}
		//FragmentShader
		if (m_fragmentpath != "")
		{
			if (compile_partial_shader(load_shader_file(m_fragmentpath).c_str(), fragmentshader, gl::FRAGMENT_SHADER))
			{
				gl::AttachShader(m_shaderprogram, fragmentshader);
			}
			else
			{
				Logger::get().write(("ERROR::STAGE_AVOIDED::" + get_name()).c_str());
			}
		}
		//ComputeShader
		if (m_computepath != "")
		{
			if (compile_partial_shader(load_shader_file(m_computepath).c_str(), computepath, gl::COMPUTE_SHADER))
			{
				gl::AttachShader(m_shaderprogram, computepath);
			}
			else
			{
				Logger::get().write(("ERROR::STAGE_AVOIDED::" + get_name()).c_str());
			}
		}

		//compile and link the shader program
		gl::LinkProgram(m_shaderprogram);
		GLint success;
		//check if the program was succesfuly created 
		{
			GLchar infoLog[1024];
			gl::GetProgramiv(m_shaderprogram, gl::LINK_STATUS, &success);
			if (!success) {
				gl::GetProgramInfoLog(m_shaderprogram, 1024, NULL, infoLog);
				mrbl::string error_msg("ERROR::SHADER::LINKER::LINK_FAILED\n ");
				error_msg += (char*)infoLog;
				Logger::get().write(error_msg.c_str());
			}
		}

		//delete the exausted vertex shader programs.
		gl::DeleteShader(vertexShader);
		gl::DeleteShader(tesselationControlShader);
		gl::DeleteShader(tesselationEvaluationShader);
		gl::DeleteShader(geometryShader);
		gl::DeleteShader(fragmentshader);
		gl::DeleteShader(computepath);
	}
}

