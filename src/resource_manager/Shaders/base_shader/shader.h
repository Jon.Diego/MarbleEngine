/*-------------------------------------------------
File: shader.h
Author: Jon Diego
Date: August 2018
Brief: Base of any Shader. Allows us to work with opengl
	   shaders easily
-------------------------------------------------*/
#pragma once

#include "containers/vector.h"
#include "containers/string.h"
#include "structural_classes/resource/resource.h"
#include "resource_manager/resource_manager.h"
#include <memory>//std::unique_ptr
#include <any>//std::any


namespace mrbl
{
	class Shader :public Resource
	{
		RTTI_DECLARATION;
	public:
		Shader(mrbl::string name, 
			mrbl::string vertexpath,
			mrbl::string tesselationcontrolpath,
			mrbl::string tesselationevaluationpath,
			mrbl::string geometrypath,
			mrbl::string fragmentpath,
			mrbl::string computepath);
		~Shader()override;
		void render(std::any data);
		int get_uniform(unsigned int index)const;
		virtual void get_uniforms() = 0;

		/***************************************************************/
		template<typename T>
		static Shader* load_shader(mrbl::string name, 
			mrbl::string vertexpath,
			mrbl::string tesselationcontrolpath,
			mrbl::string tesselationevaluationpath,
			mrbl::string geometrypath,
			mrbl::string fragmentpath,
			mrbl::string computepath)
		{
			ResourceManager::ResourceMap& manager = ResourceManager::get().get_map<Shader>();

			if (manager.find<Shader>(name.c_str()) != nullptr)
			{
				mrbl::string error_msg("ERROR::SHADER_CREATION::Shader name " + name + " already in use.\n");
				Logger::get().write(error_msg.c_str());
				
			}
			else
			{
				std::unique_ptr<T> new_shader = std::make_unique<T>(std::move(name),std::move(vertexpath), std::move(tesselationcontrolpath), std::move(tesselationevaluationpath), std::move(geometrypath), std::move(fragmentpath), std::move(computepath));
				if (new_shader)
				{
					new_shader->get_uniforms();
					Shader* new_shader_raw = new_shader.get();
					if(manager.add(std::move(new_shader)))
						return new_shader_raw;
				}
			}
			return nullptr;
		}
		/************************************************************/
		static const Shader* s_last_shader;

	private:
		void compile_shader();
		virtual void inner_render(std::any data) = 0;
		mrbl::string m_vertexpath;
		mrbl::string m_tesselationcontrolpath;
		mrbl::string m_tesselationevaluationpath;
		mrbl::string m_geometrypath;
		mrbl::string m_fragmentpath;
		mrbl::string m_computepath;
	protected:
		std::vector <int> m_uniformvector;
		unsigned int m_shaderprogram;
	};
}