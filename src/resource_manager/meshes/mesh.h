/*-------------------------------------------------
File: mesh.h
Author: Jon Diego
Date: August 2018
Brief: Stores an OpenGL mesh within it.
-------------------------------------------------*/
#pragma once
#include "structural_classes/resource/resource.h"
#include "resource_manager/resource_manager.h"
#include "containers/string.h"//mrbl::string
#include "containers/vector.h"//mrbl::vector

namespace mrbl
{
	class Mesh :public Resource
	{
		RTTI_DECLARATION;
	public:
		//CONSTRUCTORS
		Mesh(mrbl::string name, mrbl::vector<float> vertices, mrbl::vector<unsigned int> atribute_size);
		~Mesh()override;

		//METHODS
		//Binds the mesh to OpenGL
		void bind();
		//Returns the number of vertexes in the mesh.
		unsigned get_vertex_num();
		//Inits the mesh, loading it to the GPU.
		void init();
		//Returns the number of floats that the atributes of one vertex take.
		unsigned get_vertex_size();
		//Sets the GL binding to 0.
		void unbind();

		//STATIC METHODS
		//Loads the meshes from a scene.
		static Mesh* load_mesh(mrbl::string name, mrbl::vector<float> vertices, mrbl::vector<unsigned int> atribute_size);
	private:
		//METHODS
		//clears the GL objects.
		void clear_buffers();

		//STATIC DATA
		//Optimization, the last used mesh.
		static Mesh* s_last_mesh;

		//DATA
		unsigned int m_vbo;//GL VBO
		unsigned int m_vao;//GL VAO
		unsigned int m_vertexnum;//Number of vertices in the mesh.
		mrbl::vector<float> m_vertices;//Vertex info of the mesh.
		mrbl::vector<unsigned>m_atribute_size;//number of atributes and the size of each one.
	};
}
