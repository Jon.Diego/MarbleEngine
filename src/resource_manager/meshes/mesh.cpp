/*-------------------------------------------------
File: mesh.cpp
Author: Jon Diego
Date: August 2018
Brief: Stores an OpenGL mesh within it.
-------------------------------------------------*/
#include "mesh.h"
#include "GL/gl_core_4_4.hpp"//Opengl types and functions.
#include <numeric>//std::accumulate
 
namespace mrbl
{
	RTTI_IMPLEMENTATION(Mesh, &Resource::get_rtti_static());


	Mesh* Mesh::s_last_mesh = nullptr;

	Mesh::Mesh(mrbl::string name, mrbl::vector<float> vertices, mrbl::vector<unsigned int> atribute_size)
	:Resource(std::move(name)), m_vbo(gl::NONE), m_vao(gl::NONE), m_vertexnum(0), m_vertices(std::move(vertices)), m_atribute_size(std::move(atribute_size))
	{
		init();
	}

	Mesh::~Mesh()
	{
		clear_buffers();
	}

	void Mesh::bind()
	{
		if (s_last_mesh != this)
		{
			gl::BindVertexArray(m_vao);
			s_last_mesh = this;
		}
	}

	void Mesh::init()
	{
		clear_buffers();
		gl::GenVertexArrays(1, &m_vao); 
		gl::GenBuffers(1, &m_vbo);

		gl::BindVertexArray(m_vao);
		gl::BindBuffer(gl::ARRAY_BUFFER, m_vbo);
		gl::BufferData(gl::ARRAY_BUFFER, sizeof(GLfloat) * m_vertices.size(), m_vertices.data(), gl::STATIC_DRAW);

		unsigned int addall = get_vertex_size();
		unsigned int addtilnow = 0;
		unsigned int numofatrib = 0;
		for (unsigned int atbr_size : m_atribute_size)
		{
			gl::VertexAttribPointer(numofatrib, atbr_size, gl::FLOAT, gl::FALSE_, addall * sizeof(GLfloat),
				(GLvoid*)(addtilnow * sizeof(GLfloat)));
			gl::EnableVertexAttribArray(numofatrib);
			++numofatrib;
			addtilnow += atbr_size;
		}
		m_vertexnum = static_cast<unsigned int>(m_vertices.size())/ addall;
		s_last_mesh = this;
	}

	void Mesh::clear_buffers()
	{
		gl::DeleteVertexArrays(1, &m_vao);
		gl::DeleteBuffers(1, &m_vbo);
		m_vao = gl::NONE;
		m_vbo = gl::NONE;
		if (s_last_mesh == this)
		{
			s_last_mesh = nullptr;
		}
	}

	void Mesh::unbind()
	{
		gl::BindVertexArray(0);
		if (s_last_mesh == this)
			s_last_mesh = nullptr;
	}

	unsigned int Mesh::get_vertex_num()
	{
		return m_vertexnum;
	}

	unsigned int Mesh::get_vertex_size()
	{
		return std::accumulate(m_atribute_size.begin(), m_atribute_size.end(), 0u);
	}

	Mesh* Mesh::load_mesh(mrbl::string name, mrbl::vector<float> vertices, mrbl::vector<unsigned int> atribute_size)
	{
		ResourceManager::ResourceMap& manager = ResourceManager::get().get_map<Mesh>();
		if (manager.find<Mesh>(name.c_str()) != nullptr)
		{
			mrbl::string error_msg("ERROR::SHADER_CREATION::Shader name " + name + " already in use.\n");
			Logger::get().write(error_msg.c_str());
			
		}
		else
		{
			std::unique_ptr<Mesh> new_mesh = std::make_unique<Mesh>(std::move(name), std::move(vertices), std::move(atribute_size));
			if (new_mesh)
			{
				Mesh* new_mesh_raw = new_mesh.get();
				if (manager.add(std::move(new_mesh)))
					return new_mesh_raw;
			}
		}
		return nullptr;
	}
}