/*-------------------------------------------------
File: resource_manager.h
Author: Jon Diego
Date: August 2018
Brief: This class stores and manages all the resources
of the game.
-------------------------------------------------*/
#pragma once
#include "containers/unordered_map.h"//mrbl::unordered_map
#include "containers/map.h"//mrbl::map
#include "structural_classes/rtti/rtti.h"//mrbl::rtti
#include "structural_classes/resource/resource.h"//mrbl::Resource
#include <memory>//std::unique_ptr
#include "logger/logger.h"

namespace mrbl
{
	class ResourceManager
	{
	public:
		//Stores all the resources of a Type.
		class ResourceMap
		{
		public:
			//Looks for a resource with name "name" returns it if found.
			template<typename T>
			T* find(const char * name);
			
			//Adds a resource. It may Overwrite any previously existing resource with the same name.
			bool add(std::unique_ptr<Resource> resource);
			
			//Adds a resource. It does not add it if there already exists another one with the same name.
			bool add_if_new(std::unique_ptr<Resource> resource);
			
			//Removes a resource by name.
			bool remove(const char * name);
			
			//Renames a resource. If the new name already exist it will erase the old ocurrence.
			bool rename(const char* old_name, const char * new_name);
			
			//Renames a resource. If the new name already exist it will do no change.
			bool rename_if_new(const char* old_name, const char * new_name);
			
			//Clears all the resources of the ResourceMap
			void clear();
			
			//Returns the begin iterator of the container.
			mrbl::unordered_map<mrbl::string, std::unique_ptr<Resource>>::iterator get_start_iterator();
			
			//Returns the end iterator of the container.
			mrbl::unordered_map<mrbl::string, std::unique_ptr<Resource>>::const_iterator get_end_iterator();
			
			//Checks if the ResourceMap is empty.
			bool empty()const;
			
			//Returns The number of resources in the ResourceMap.
			size_t size()const;
		private:
			mrbl::unordered_map<mrbl::string, std::unique_ptr<Resource>> m_umap;
		};
		//Constructor/Destructor
		ResourceManager();
		~ResourceManager();

		//Returns a reference to the resource map that stores the resources of type T.
		//If the Resource did not exist it creates it.
		template<typename T>
		ResourceMap& get_map();

		//Clears all the resources of every type.
		void clear_all_resources();

		//Singleton get.
		static ResourceManager& get();


	private:
		//Container that stores every resource in the game.
		mrbl::map <const RTTI*, ResourceMap> m_resource_map;

		//You cannot copy or move a singleton.
		ResourceManager(const ResourceManager& rhs) = delete;
		ResourceManager(const ResourceManager&& rhs) = delete;
		ResourceManager& operator= (const ResourceManager& rhs) = delete;
		ResourceManager& operator= (const ResourceManager&& rhs) = delete;

		//singleton pointer.
		static ResourceManager* s_manager;
	};
}
#include "resource_manager.inl"

