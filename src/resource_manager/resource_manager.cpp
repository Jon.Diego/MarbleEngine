/*-------------------------------------------------
File: resource_manager.cpp
Author: Jon Diego
Date: August 2018
Brief: This class stores and manages all the resources
of the game.
-------------------------------------------------*/

#include "resource_manager.h"
#include "exception/exception.h"
#include "logger/logger.h"

namespace mrbl
{
	//singleton pointer.
	ResourceManager*  ResourceManager::s_manager = nullptr;
	
	/*--------------------------------------
	brief: Singleton get.
	return:
		Reference to the resource manager.
	--------------------------------------*/
	ResourceManager& ResourceManager::get()
	{
		return *s_manager;
	}
	
	/*--------------------------------------
	brief: Constructor
		throws if a second Resource manager is instantiated.
	--------------------------------------*/
	ResourceManager::ResourceManager()
	{
		if (s_manager != nullptr)
			throw(Exception("Tried to instantiate a second Resource Manager."));
		s_manager = this;
	}
	
	/*--------------------------------------
	brief: Destructor
	--------------------------------------*/
	ResourceManager::~ResourceManager()
	{
		clear_all_resources();
	
		if (s_manager != this)
			Logger::get().write("Resource Manager pointer was changed before deletion");
		else
			s_manager = nullptr;
	}
	
	/*--------------------------------------
	brief: Clears all the resources of every type.
	--------------------------------------*/
	void ResourceManager::clear_all_resources()
	{
		m_resource_map.clear();
	}
	
	/*--------------------------------------
	brief: Adds a resource. It may Overwrite any previously existing resource with the same name.
	params:
		resource: The resource to add.
	return:
		true if the resource was added, false otherwise.
	--------------------------------------*/
	bool ResourceManager::ResourceMap::add(std::unique_ptr<Resource> resource)
	{
		if (resource == nullptr)
			return false;
	
		m_umap.emplace(resource->get_name(), std::move(resource));
		return true;
	}
	
	/*--------------------------------------
	brief: Adds a resource. It does not add it if there already exists another one with the same name.
	params:
		resource: the resource to add.
	return:
		true if the resource was added, false otherwise.
	--------------------------------------*/
	bool ResourceManager::ResourceMap::add_if_new(std::unique_ptr<Resource> resource)
	{
		if (resource == nullptr)
			return false;
	
		if (m_umap.find(resource->get_name()) != m_umap.end())
			return false;
	
		m_umap.emplace(resource->get_name(), std::move(resource));
		return true;
	}
	
	/*--------------------------------------
	brief: Removes a resource by name.
	params:
		name: the name of the resource to be removed.
	return:
		true if a resource with that name existed and was removed, false otherwise.
	--------------------------------------*/
	bool ResourceManager::ResourceMap::remove(const char * name)
	{
		return m_umap.erase(name);
	}
	
	/*--------------------------------------
	brief: Renames a resource. If the new name already exist it will erase the old ocurrence.
	params:
		old_name: Current name of the resource.
		new_name: The new name that will be given to the resource.
	return:
		true if a resource was renamed false otherwise.
	--------------------------------------*/
	bool ResourceManager::ResourceMap::rename(const char* old_name, const char * new_name)
	{
		mrbl::unordered_map<mrbl::string, std::unique_ptr<Resource>>::iterator itr = m_umap.find(old_name);
		if (itr != m_umap.end())
		{
			std::unique_ptr<Resource> temp = std::move(itr->second);
			m_umap.erase(itr);
			temp->set_name(new_name);
			m_umap.emplace(std::make_pair(new_name, std::move(temp)));
			return true;
		}
		return false;
	}
	
	/*--------------------------------------
	brief: Renames a resource. If the new name already exist it will do no change.
	params:
		old_name: Current name of the resource.
		new_name: The new name that will be given to the resource.
	return:
		true if a resource was renamed false otherwise.
	--------------------------------------*/
	bool ResourceManager::ResourceMap::rename_if_new(const char* old_name, const char * new_name)
	{
		mrbl::unordered_map<mrbl::string, std::unique_ptr<Resource>>::iterator itr = m_umap.find(old_name);
		mrbl::unordered_map<mrbl::string, std::unique_ptr<Resource>>::iterator itr_new = m_umap.find(new_name);
		if (itr != m_umap.end() && itr_new == m_umap.end())
		{
			std::unique_ptr<Resource>&& temp = std::move(itr->second);
			m_umap.erase(itr);
			temp->set_name(new_name);
			m_umap.emplace(std::make_pair(new_name, std::move(temp)));
			return true;
		}
		return false;
	}
	
	/*--------------------------------------
	brief: Clears all the resources of the ResourceMap
	--------------------------------------*/
	void ResourceManager::ResourceMap::clear()
	{
		m_umap.clear();
	}
	
	/*--------------------------------------
	brief: Returns the begin iterator of the container.
	return:
		begin iterator of the container.
	--------------------------------------*/
	mrbl::unordered_map<mrbl::string, std::unique_ptr<Resource>>::iterator ResourceManager::ResourceMap::get_start_iterator()
	{
		return m_umap.begin();
	}
	
	/*--------------------------------------
	brief: Returns the end iterator of the container.
	return:
		const end iterator of the container.
	--------------------------------------*/
	mrbl::unordered_map<mrbl::string, std::unique_ptr<Resource>>::const_iterator ResourceManager::ResourceMap::get_end_iterator()
	{
		return m_umap.end();
	}
	
	/*--------------------------------------
	brief: Checks if the ResourceMap is empty.
	return:
		true if it is emtpy false otherwise.
	--------------------------------------*/
	bool ResourceManager::ResourceMap::empty() const
	{
		return m_umap.empty();
	}
	
	/*--------------------------------------
	brief: Returns The number of resources in the ResourceMap.
	return:
		The number of elements in the resource map.
	--------------------------------------*/
	size_t ResourceManager::ResourceMap::size() const
	{
		return m_umap.size();
	}
}