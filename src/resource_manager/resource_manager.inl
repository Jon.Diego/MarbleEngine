/*-------------------------------------------------
File: resource_manager.inl
Author: Jon Diego
Date: August 2018
Brief: This class stores and manages all the resources
of the game.
-------------------------------------------------*/

namespace mrbl
{
	/*--------------------------------------
	brief: Returns a reference to the resource map that stores the resources of type T.
	return:
		A reference to the resource map.
	--------------------------------------*/
	template<typename T>
	ResourceManager::ResourceMap& ResourceManager::get_map()
	{
		return m_resource_map[&T::get_rtti_static()];
	}


	/*--------------------------------------
	brief: Looks for a resource with name "name" returns it casted to a pointer of T if found.
	params:
		name: The name of the resource we are looking for.
	return:
		Pointer to the found resource casted to T*, nullptr if it was not found.
	--------------------------------------*/
	template<typename T>
	T* ResourceManager::ResourceMap::find(const char * name)
	{
		mrbl::unordered_map<mrbl::string, std::unique_ptr<Resource>>::iterator itr = m_umap.find(name);
		if (itr != m_umap.end())
			return static_cast<T*>(itr->second.get());
		return nullptr;
	}
}