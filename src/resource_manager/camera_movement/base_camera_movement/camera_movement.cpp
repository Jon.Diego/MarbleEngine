/*------------------------------------------------
File: camera_behaviour.cpp
Author: Jon Diego
Date: November 2018
Brief: Base class for camera behaviours.
	   All camera behaviours will inherit from it.
-------------------------------------------------*/
#include "camera_movement.h"

namespace mrbl
{
	RTTI_IMPLEMENTATION(CameraMovement, &Resource::get_rtti_static());
}