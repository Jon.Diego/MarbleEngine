/*------------------------------------------------
File: camera_behaviour.h
Author: Jon Diego
Date: November 2018
Brief: Base class for camera behaviours.
	   All camera behaviours will inherit from it.
-------------------------------------------------*/
#pragma once
#include "structural_classes/resource/resource.h"

namespace mrbl
{
	class Transform;

	class CameraMovement :public Resource
	{
		RTTI_DECLARATION;
	public:
		virtual void update(Transform& transform, float dt) = 0;
	};
}