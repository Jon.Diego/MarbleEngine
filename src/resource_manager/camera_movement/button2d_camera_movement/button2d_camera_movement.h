/*------------------------------------------------
File: button_2d_camera_movement.h
Author: Jon Diego
Date: November 2018
Brief: Simple 2D Camera Behaviour Pressing buttons 
	   Moves it LeftRightUp and Down.
-------------------------------------------------*/
#pragma once
#include "../base_camera_movement/camera_movement.h"

namespace mrbl
{
	class Button2DCameraMovement: public CameraMovement
	{
		RTTI_DECLARATION;
	public:
		void update(Transform& transform, float dt) override;
	};
}