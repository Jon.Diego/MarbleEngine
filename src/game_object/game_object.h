/*-------------------------------------------------
File: game_object.h
Author: Jon Diego
Date: June 2018
Brief: An object located on the 3D world.
	   What it really is will be defined by it's components.
-------------------------------------------------*/
#pragma once

#include "structural_classes/base/base.h"//Base
#include "structural_classes/rtti/rtti.h"//RTTI
#include "transform/transform.h"//transform
#include "containers/vector.h"//mrbl::vector
#include <GLM/vec3.hpp>//glm::fvec3
#include <GLM/gtc/quaternion.hpp>//glm::fquat

namespace mrbl
{
	//Predefinitions
	class Component;

	class GameObject: public Base
	{
		RTTI_DECLARATION;
	public:
		using component_element = std::pair < RTTI::count_type, std::unique_ptr<Component>>;
		using component_container = mrbl::vector <component_element>;
		//Constructors
		GameObject(glm::fvec3 position = glm::fvec3(0.f, 0.f, 0.f), glm::fvec3 scale = glm::fvec3(1.f, 1.f, 1.f), glm::fquat rotation = glm::fquat(1.f, 0.f, 0.f, 0.f), GameObject * parent = nullptr);
		~GameObject() override  = default;
		//Statics
		static const GameObject& get_game_object_from_transform(const Transform& transform);
		static GameObject& get_game_object_from_transform(Transform& transform);
		//Methods
		GameObject* get_parent()const;
		void set_parent(GameObject* parent);

		/*Component Management********************************/
		Component* add_component(std::unique_ptr<Component>&& comp);
		size_t get_component_total_num();
		//returns the begin iterator of the component container.
		component_container::iterator get_start_iterator();
		//returns the end iterator of the component container.
		component_container::const_iterator get_end_iterator() const;
		//Returns the first component of the templated type. Nullptr otherwise.
		template<typename T>
		T* get_component();
		//Returns the number of components of the templated type.
		template<typename T>
		size_t get_component_num();
		//Returns an iterator to the next component of the templated type. 
		//It can return the same iterator that was provided if it is of the selected type.
		template<typename T>
		component_container::iterator get_next_component_of_type(component_container::iterator it);
		/*****************************************************/
		//Data
		Transform m_transform;
	private:
		component_container m_components;
	};

	/*--------------------------------------
	brief: Returns the first component of the templated type.
	return:
		pointer to the first component of the templated type. Nullptr if there is none.
	--------------------------------------*/
	template<typename T>
	T* GameObject::get_component()
	{
		RTTI::count_type type_id = T::get_rtti_static().get_id();
		for (component_element& comp_pair : m_components)
		{
			if (comp_pair.first == type_id)
				return static_cast<T*>(comp_pair.second);
		}
		return nullptr;
	}

	/*--------------------------------------
	brief: Returns the number of components of the templated type.
	return:
		Number of components of the selected type.
	--------------------------------------*/
	template<typename T>
	size_t GameObject::get_component_num()
	{
		RTTI::count_type type_id = T::get_rtti_static().get_id();
		size_t counter = 0;
		for (component_element& comp_pair : m_components)
		{
			if (comp_pair.first == type_id)
				++counter;
		}
		return counter;
	}

	/*--------------------------------------
	brief: Returns an iterator to the next component of the templated type. 
		   It can return the same iterator that was provided if it is of the selected type.
	return:
		   Iterator to the component, it will point to the end if none is remaining.
	--------------------------------------*/
	template<typename T>
	GameObject::component_container::iterator GameObject::get_next_component_of_type(GameObject::component_container::iterator it)
	{
		RTTI::count_type type_id = T::get_rtti_static().get_id();
		for (;it != m_components.end();++it)
		{
			if (it->first == type_id)
				return it;
		}
		return m_components.end();
	}
}


