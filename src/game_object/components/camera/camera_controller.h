/*-------------------------------------------------
File: camera_controller.h
Author: Jon Diego
Date: September 2018
Brief: This component controls the movement of a camera
	   GameObject.
-------------------------------------------------*/
#pragma once
#include "structural_classes/component/component.h"

namespace mrbl
{
	class CameraController : public Component
	{
		RTTI_DECLARATION;
	public:
		//Constructor and assingment operators.
		CameraController();
		CameraController(float movement_speed, float rotation_speed);
		CameraController(const CameraController& other);
		CameraController(CameraController&& other);

		CameraController& operator=(const CameraController& other);
		CameraController& operator=(CameraController&& other);

		//Settors gettors.
		float get_speed();
		void set_speed(float speed);
		
		//Update
		void update(float dt) override;

	private:
		float m_movement_speed;
		float m_rotation_speed;
	};
}