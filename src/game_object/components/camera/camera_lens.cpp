/*-------------------------------------------------
File: camera_lens.cpp
Author: Jon Diego
Date: September 2018
Brief: This component controls the projection matrix of a camera.
-------------------------------------------------*/
#include "camera_lens.h"

namespace mrbl
{
	RTTI_IMPLEMENTATION(CameraLens, &Component::get_rtti_static());

	void CameraLens::update(float /*dt*/)
	{

	}
}