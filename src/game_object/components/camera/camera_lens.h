#pragma once
/*-------------------------------------------------
File: camera_lens.h
Author: Jon Diego
Date: September 2018
Brief: This component controls the projection matrix of a camera.
-------------------------------------------------*/
#pragma once
#include "structural_classes/component/component.h"

namespace mrbl
{
	class CameraLens : public Component
	{
		RTTI_DECLARATION;
	public:
		void update(float dt) override;
	};
}

