/*-------------------------------------------------
File: camera_controller.cpp
Author: Jon Diego
Date: September 2018
Brief: This component controls the movement of a camera
	   GameObject.
-------------------------------------------------*/

#include "camera_controller.h"
#include "game_object/game_object.h"//GameObject
#include "media_layer/input_system/input_system.h"//InputSystem


namespace mrbl
{
	RTTI_IMPLEMENTATION(CameraController, &Component::get_rtti_static());

	//Constructor and assingment operators.

	CameraController::CameraController()
		:m_movement_speed(1.f), m_rotation_speed(1.f)
	{}

	CameraController::CameraController(float movement_speed, float rotation_speed)
		:m_movement_speed(movement_speed), m_rotation_speed(rotation_speed)
	{}

	CameraController::CameraController(const CameraController& other)
		:m_movement_speed(other.m_movement_speed), m_rotation_speed(other.m_rotation_speed)
	{}

	CameraController::CameraController(CameraController&& other)
		:m_movement_speed(other.m_movement_speed), m_rotation_speed(other.m_rotation_speed)
	{}

	CameraController& CameraController::operator=(const CameraController& other)
	{
		m_movement_speed = other.m_movement_speed;
		m_rotation_speed = other.m_rotation_speed;
		return *this;
	}

	CameraController& CameraController::operator=(CameraController&& other)
	{
		m_movement_speed = other.m_movement_speed;
		m_rotation_speed = other.m_rotation_speed;
		return *this;
	}

	float CameraController::get_speed()
	{
		return m_movement_speed;
	}
	
	void CameraController::set_speed(float speed)
	{
		m_movement_speed = speed;
	}

	void CameraController::update(float /*dt*/)
	{
		//Transform& trans = this->get_owner()->m_transform;
		InputSystem& input_sys = InputSystem::get();

		if (input_sys.check_input_action("TurnUp"))
			printf("TurnUp");
		if (input_sys.check_input_action("TurnLeft"))
			printf("TurnLeft");
		if (input_sys.check_input_action("TurnDown"))
			printf("TurnDown");
		if (input_sys.check_input_action("TurnRight"))
			printf("TurnRight");
		if (input_sys.check_input_action("Forward"))
			printf("Forward");
		if (input_sys.check_input_action("Backard"))
			printf("Backard");
	}
}