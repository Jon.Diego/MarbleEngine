/*-------------------------------------------------
File: game_object.cpp
Author: Jon Diego
Date: June 2018
Brief: An object located on the 3D world.
What it really is will be defined by it's components.
-------------------------------------------------*/
#include "game_object.h"
#include "structural_classes/component/component.h"

namespace
{
	/*--------------------------------------
	brief: Returns the pointer distance between a game_object and it's transform. 
		   This way the transform parent can be used to find the parent game_object.
	parameters:
		obj: An object get the distance from, can even be nullptr.
	return:
		Distance from a gameobject to its transform.
	--------------------------------------*/
	size_t get_offset_to_transform(const mrbl::GameObject* obj)
	{
		return (const char*)(&(obj->m_transform)) - (const char*)obj;
	}
}
namespace mrbl
{
	RTTI_IMPLEMENTATION(GameObject, &Base::get_rtti_static());

	/*--------------------------------------
	brief: Constructor.
	parameters:
		position: The translation of the object.
		scale: Scale of the object.
		rotation: Rotation of the object.
		parent: Parent of the object.
	--------------------------------------*/
	GameObject::GameObject(glm::fvec3 position, glm::fvec3 scale, glm::fquat rotation, GameObject* parent)
		: m_transform(position, scale, rotation, parent ? &parent->m_transform : nullptr)
	{}

	/*--------------------------------------
	brief: Get the game_object associated to a transform. 
	       Make sure that the transform is asociated to an object though.
	parameters:
		transform: The transform to take the object from.
	return:
		Reference to the object that owns the transform.
	--------------------------------------*/
	const GameObject& GameObject::get_game_object_from_transform(const Transform& transform)
	{
		const size_t offset = get_offset_to_transform(nullptr);
		return *((const GameObject*)((const char*)&transform - offset));
	}

	/*--------------------------------------
	brief: Get the game_object associated to a transform.
	Make sure that the transform is asociated to an object though.
	parameters:
		transform: The transform to take the object from.
	return:
		The gameobject attached to a transform.
	--------------------------------------*/
	GameObject& GameObject::get_game_object_from_transform(Transform& transform)
	{
		return const_cast<GameObject&> (get_game_object_from_transform(const_cast<const Transform&>(transform)));
	}

	/*--------------------------------------
	brief: Gets the parent of the game_object. nullptr if there is none.
	return:
		ptr to the parent game_object.
	--------------------------------------*/
	GameObject* GameObject::get_parent()const
	{
		Transform* parent = m_transform.get_parent();
		return parent ? &get_game_object_from_transform(*parent) : nullptr;
	}

	/*--------------------------------------
	brief: Set the parent of the game_object.
	parameters:
		parent: The game_object to set.
	--------------------------------------*/
	void GameObject::set_parent(GameObject* parent) 
	{
		m_transform.set_parent((parent)?&parent->m_transform : nullptr);
	}

	/*--------------------------------------
	brief: Adds a component to the component container.
	parameters:
		comp: The component to add. Will not be added if nullptr.
	return:
		A pointer to the component.
	--------------------------------------*/
	Component* GameObject::add_component(std::unique_ptr<Component>&& comp)
	{
		if (!comp)
			return nullptr;
		else
		{
			comp->set_owner(this);

			m_components.emplace_back(comp->get_rtti().get_id(),std::forward<std::unique_ptr<Component>&&>(comp));

			return m_components.back().second.get();
		}
	}

	/*--------------------------------------
	brief: Returns the total number of components in the gameobj.
	return:
		The number of components in the gameobj.
	--------------------------------------*/
	size_t GameObject::get_component_total_num()
	{
		return m_components.size();
	}

	/*--------------------------------------
	brief: returns the begin iterator of the component container.
	return:
		An iterator to the first element.
	--------------------------------------*/
	GameObject::component_container::iterator GameObject::get_start_iterator()
	{
		return m_components.begin();
	}

	/*--------------------------------------
	brief: returns the end iterator of the component container.
	return:
		Const iterator to the last element.
	--------------------------------------*/
	GameObject::component_container::const_iterator GameObject::get_end_iterator() const
	{
		return m_components.cend();
	}
}
