/*-------------------------------------------------
File: logger.cpp
Author: Jon Diego
Date: May 2018
Brief: Logger class, can be used to log from anywhere
	   in the program.
-------------------------------------------------*/
#include "logger.h"
#include <experimental/filesystem>

//Max size of the log path.
static const unsigned log_path_lenght = 32;

namespace mrbl
{
	namespace fs = std::experimental::filesystem;

	Logger* Logger::s_logger = nullptr;
	
	/*--------------------------------------
	brief: Constructor, opens the correct log file.
	--------------------------------------*/
	Logger::Logger()
	{
		//Save the instance for the singleton.
		s_logger = this;
		char log_path[log_path_lenght];
		//The log index to be written.
		unsigned index = 0;
		//Ensure that the directory exists.
		snprintf(log_path, log_path_lenght, "./logs");
		const fs::path directory_path(log_path);
		//Create the directory if it does not exist.
		if (!fs::exists(directory_path))
		{
			fs::create_directory(directory_path);
		}
		//Check the log number that must be written
		//Write the first one that does not exists.
		//Select the oldest one otherwise.
		else
		{
			fs::file_time_type time;
			for (unsigned i = 0; i < max_log_num; ++i)
			{
				snprintf(log_path, log_path_lenght, "./logs/mrbl_log_%u.txt", i);
				const fs::path path(log_path);
				if (fs::exists(path))
				{
					fs::file_time_type temp_time = fs::last_write_time(path);
					if (i == 0)
						time = temp_time;
					else if (temp_time < time)
					{
						index = i;
						time = temp_time;
					}
				}
				else
				{
					index = i;
					break;
				}
			}
		}
		//Open the file.
		if (log_path_lenght <= snprintf(log_path, log_path_lenght, "./logs/mrbl_log_%u.txt", index))
		{
			write("Main Log path name is too big for the path buffer.");
		}
		m_file.open(log_path, std::ofstream::out|std::ofstream::trunc);
	}

	/*--------------------------------------
	brief: Destructor, flushes the string to the file.
	--------------------------------------*/
	Logger::~Logger()
	{
		flush_log();
	}

	/*--------------------------------------
	brief: Allows to access the logger from anywhere in the program.
	return:
		return the logger instance.
	--------------------------------------*/
	Logger& Logger::get()
	{
		return *s_logger;
	}

	/*--------------------------------------
	brief: flushes the stored string to the file.
	--------------------------------------*/
	void Logger::flush_log()
	{
		if (!m_buffer.empty())
		{
			m_file.write(m_buffer.c_str(), m_buffer.size());
			m_buffer.clear();
		}
	}

	/*--------------------------------------
	brief: writes to the stored string.
	parameter:
		str: The str to write.
	--------------------------------------*/
	void Logger::write(const char* str)
	{
#if PRINT_TO_CONSOLE
		printf("%s\n",str);
#endif
		m_buffer += str;
		m_buffer += "\n";
	}

	/*--------------------------------------
	brief: First writes to the string, 
		   then flushes the string to the log file.
	parameter:
		str: The str to write.
	--------------------------------------*/
	void Logger::write_and_flush(const char* str)
	{
		write(str);
		flush_log();
	}

	/*--------------------------------------
	brief: Checks if the log file is correctly open.
	return:
		true if correctly open, false otherwise.
	--------------------------------------*/
	bool Logger::is_correctly_formed()
	{
		return m_file.is_open();
	}
}