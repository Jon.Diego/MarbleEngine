/*-------------------------------------------------
File: logger.h
Author: Jon Diego
Date: May 2018
Brief: Logger class, can be used to log from anywhere
	   in the program.
-------------------------------------------------*/
#pragma once
#include "containers/string.h"	//mrbl::string
#include <fstream>
#define PRINT_TO_CONSOLE 1 //Whether log msgs should be printed directly to console or not.

namespace mrbl
{
	//max number of log files that will be rotated.
	constexpr unsigned max_log_num = 10;

	/*--------------------------------------
	Logger class, can be used to log from anywhere
	in the program.
	--------------------------------------*/
	class Logger
	{
	public:
		//CODE
		//Constructor, destructor.
		Logger();
		~Logger();
		//Access:
		static Logger& get();
		//METHODS
		//flushes the stored string to the file.
		void flush_log();
		//writes to the stored string.
		void write(const char* str);
		//First writes to the string, then flushes the string to the log file.
		void write_and_flush(const char* str);
		//Checks if the log file is correctly open.
		bool is_correctly_formed();

	private:		
		//DATA
		static Logger* s_logger;
		std::fstream m_file;
		mrbl::string m_buffer;
	};
}
