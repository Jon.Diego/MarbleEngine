/*-------------------------------------------------
File: rtti.h
Author: Jon Diego
Date: June 2018
Brief: Provides a custom run time information class
	   specially useful for serialization purposes.
-------------------------------------------------*/
#pragma once
#include "containers/string.h"//mrbl::string

namespace mrbl
{
	/*--------------------------------------
	RTTI class, makes it possible to know
	the run time type of an owning class.
	--------------------------------------*/
	class RTTI
	{
	public:
		using count_type = unsigned short;
		//CODE
		//Constructor
		RTTI(mrbl::string name, const RTTI * ptr_base_type);
		
		//Returns the type name of the class.
		const mrbl::string & get_name() const;

		//Compares the rttis of two classes.
		bool is_equal(const RTTI & other_type) const;

		//Checks if the class is derived or equal from the rhs class.
		bool is_derived(const RTTI & other_type) const;

		//Sintactic sugar for is equal.
		bool operator==(const RTTI & rhs) const;

		//Returns the numerical ID of the class(may change every run).
		count_type get_id() const;

		//STATIC
		//returns the number of RTTI types there is:
		count_type get_total_number_of_classes();

	private:
		//STATICS:
		count_type class_count_ = 0;
		//DATA
		//Name of the class.
		const mrbl::string m_name;
		//Pointer to the parent RTTI.
		const RTTI *m_ptr_base_type;
		//ID of the class.
		count_type m_id;
	};
}

/*****************************************************************************/
// MACROS
// include this macro at the top of each class.
#define RTTI_DECLARATION														\
public:																			\
	virtual const RTTI& get_rtti() const;                                       \
	static  const RTTI& get_rtti_static();                                      \
private:																		\
	static  const RTTI s_type                                         

// inlude this other macro in the .cpp
#define RTTI_IMPLEMENTATION(thisType, parentTypeAddr)							\
	const RTTI& thisType::get_rtti() const										\
	{																			\
		return get_rtti_static();									            \
	}		                                                                    \
	const RTTI& thisType::get_rtti_static()                                     \
	{                                                                           \
		return s_type;															\
	}                                                                           \
	const RTTI thisType::s_type = {typeid(thisType).name(), parentTypeAddr}	
/*****************************************************************************/
