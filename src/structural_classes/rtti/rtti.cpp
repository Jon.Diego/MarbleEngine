/*-------------------------------------------------
File: rtti.cpp
Author: Jon Diego
Date: June 2018
Brief: Provides a custom run time information class
specially useful for serialization purposes.
-------------------------------------------------*/
#include "rtti.h"

namespace mrbl
{
	/*--------------------------------------
	brief: Constructor, creates and populates an RTTI class.
	parameters:
		name: The name of the class for rtti purposes.
		ptr_base_type: pointer to the rtti parent of the class.(should be nullptr if base class)
	--------------------------------------*/
	RTTI::RTTI(mrbl::string name, const RTTI * ptr_base_type) :
		m_name(std::move(name)), m_ptr_base_type(ptr_base_type), m_id(class_count_++)
	{}

	/*--------------------------------------
	brief: gettor for the rtti name.
	return:
		A string containing the name.
	--------------------------------------*/
	const mrbl::string & RTTI::get_name() const
	{
		return m_name;
	}

	/*--------------------------------------
	brief:	compares two rttis to check if they are the same.
	parameters:
		other_type: The other rtti to compare with.
	return:
		true if both rttis are equivalent false otherwise.
	--------------------------------------*/
	bool RTTI::is_equal(const RTTI & other_type) const
	{
		return this == &other_type;
	}

	/*--------------------------------------
	brief: checks if the current rtti is equal or derived from the
		   rhs rtti.
	other_type: The other rtti to compare with.
	return:
		true if equal or derived.
	--------------------------------------*/
	bool RTTI::is_derived(const RTTI & other_type) const
	{
		const RTTI* temp = this;

		// walk up in the hierarchy
		while (temp)
		{
			if (*temp == other_type)
				return true;

			temp = temp->m_ptr_base_type;
		}
		return false;
	}

	/*--------------------------------------
	brief: sintactic sugar for is_equal.
	--------------------------------------*/
	bool RTTI::operator==(const RTTI & rhs) const
	{
		return is_equal(rhs);
	}

	/*--------------------------------------
	brief: Returns the numerical ID of the class(may change every run).
	return:
		The numerical ID of the class.
	--------------------------------------*/
	//
	RTTI::count_type RTTI::get_id() const
	{
		return m_id;
	}

	//STATIC
	/*--------------------------------------
	brief: returns the number of RTTI types there is
	return:
		The number of classes that have rtti.
	--------------------------------------*/
	RTTI::count_type RTTI::get_total_number_of_classes()
	{
		return class_count_;
	}
}
