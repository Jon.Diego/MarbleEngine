/*-------------------------------------------------
File: resource.cpp
Author: Jon Diego
Date: July 2018
Brief: Base of any resource. Any data contained outside
of the code can be considered a resource.
-------------------------------------------------*/

#include "resource.h"

namespace mrbl
{
	RTTI_IMPLEMENTATION(Resource, &Base::get_rtti_static());

	/*--------------------------------------
	brief: Constructor, the base resource only holds its name.
	--------------------------------------*/
	Resource::Resource(mrbl::string name):m_name(std::move(name))
	{}

	/*--------------------------------------
	brief: Returns the name of the resource.
	return:
		Reference to the name of the resource.
	--------------------------------------*/
	const mrbl::string& Resource::get_name()const
	{
		return m_name;
	}


	/*--------------------------------------
	brief: Changes the name of the resource
	params:
		new_name: The new name the resource will have.
	--------------------------------------*/
	void Resource::set_name(const char* new_name)
	{
		m_name = std::move(new_name);
	}
}