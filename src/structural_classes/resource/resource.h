/*-------------------------------------------------
File: resource.h
Author: Jon Diego
Date: July 2018
Brief: Base of any resource. Any data contained outside
       of the code can be considered a resource.
-------------------------------------------------*/
#pragma once

#include "structural_classes/base/base.h"//Base
#include "containers/string.h"


namespace mrbl
{
	class Resource : public Base
	{
		RTTI_DECLARATION;
	public:
		//Constructor
		Resource(mrbl::string name);
		~Resource() override = default;

		//Getor
		const mrbl::string & get_name() const;
		friend class ResourceManager;
	private:
		Resource(Resource& other) = delete;
		Resource(Resource&& other) = delete;
		Resource& operator = (Resource& other) = delete;
		Resource& operator = (Resource&& other) = delete;
		void set_name(const char* new_name);
		mrbl::string m_name;
	};
}