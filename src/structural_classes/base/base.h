/*-------------------------------------------------
File: base.h
Author: Jon Diego
Date: June 2018
Brief: Base class of the project. It's aim is to
	   be the spine of the class hierarchy. 
-------------------------------------------------*/
#pragma once
#include "structural_classes/rtti/rtti.h"

namespace mrbl
{
	/*--------------------------------------
	Base class, spine of gameobjects and components.
	--------------------------------------*/
	class Base
	{
		RTTI_DECLARATION;
	public:
		Base() = default;
		virtual ~Base() = default;
		Base(const Base&) = default;
		Base(Base&&) = default;
	};
}