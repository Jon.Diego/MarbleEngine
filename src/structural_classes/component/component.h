/*-------------------------------------------------
File: component.h
Author: Jon Diego
Date: July 2018
Brief: Base of any component. Components are owned by
	   objects and define what they are by composition.
-------------------------------------------------*/
#pragma once
#include "structural_classes/rtti/rtti.h"
#include "structural_classes/base/base.h"


namespace mrbl
{
	//Predefinitions
	class GameObject;

	class Component : public Base
	{
		RTTI_DECLARATION;
	public:
		//CONSTRUCTORS
		Component();
		~Component() override = default;
		//METHODS
		GameObject* get_owner();
		void set_owner(GameObject * owner);
		virtual void update(float dt) = 0;
	private:
		GameObject * m_owner;
	};
}