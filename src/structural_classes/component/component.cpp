/*-------------------------------------------------
File: component.cpp
Author: Jon Diego
Date: July 2018
Brief: Base of any component. Components are owned by
objects and define what they are by composition.
-------------------------------------------------*/

#include "component.h"

namespace mrbl
{
	RTTI_IMPLEMENTATION(Component, &Base::get_rtti_static());

	/*--------------------------------------
	brief: Constructor, Owner is set to null because it will be
		   populated once the component is added to an object.
	--------------------------------------*/
	Component::Component():m_owner(nullptr)
	{}

	/*--------------------------------------
	brief: Returns a pointer to the components onwer. Nullptr if there is none.
	return:
		The pointer to the owner.
	--------------------------------------*/
	GameObject* Component::get_owner()
	{
		return m_owner;
	}

	/*--------------------------------------
	brief: Sets the value of the game object that owns this component.
	parameters:
		onwer: Pointer to the owner of this component. nullptr if there is none.
	--------------------------------------*/
	void Component::set_owner(GameObject * owner)
	{
		m_owner = owner;
	}
}