#pragma once
#include <vector>

namespace mrbl
{
	template<typename T>
	using vector = std::vector<T>;
}