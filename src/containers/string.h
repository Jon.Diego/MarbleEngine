/*-------------------------------------------------
File: string.h
Author: Jon Diego
Date: May 2018
Brief: typedefinition of an std::string.
Used to make sure that we are using our custom allocators.
-------------------------------------------------*/
#pragma once
#include <string>

namespace mrbl
{
	using string = std::basic_string<char, std::char_traits<char>>;

	template<typename T>
	using basic_string = std::basic_string<T, std::char_traits<T>>;
}