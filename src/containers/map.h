/*-------------------------------------------------
File: map.h
Author: Jon Diego
Date: May 2018
Brief: typedefinition of an std::map.
Used to make sure that we are using our custom allocators.
-------------------------------------------------*/
#pragma once
#include <map>

namespace mrbl
{
	template<typename Key, typename T>
	using map = std::map<Key,T>;
}
