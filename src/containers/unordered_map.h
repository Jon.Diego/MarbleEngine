/*-------------------------------------------------
File: map.h
Author: Jon Diego
Date: May 2018
Brief: typedefinition of an std::unordered_map/multimap.
Used to make sure that we are using our custom allocators.
-------------------------------------------------*/
#pragma once
#include <unordered_map>

namespace mrbl
{
	template<typename Key, typename T>
	using unordered_map = std::unordered_map<Key,T>;

	template<typename Key, typename T>
	using unordered_multimap = std::unordered_multimap<Key, T>;
}