/*-------------------------------------------------
File: list.h
Author: Jon Diego
Date: May 2018
Brief: typedefinition of an std::list.
Used to make sure that we are using our custom allocators.
-------------------------------------------------*/
#pragma once
#include <list>

namespace mrbl
{
	template<typename T>
	using list = std::list<T>;
}