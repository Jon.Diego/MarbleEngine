/*-------------------------------------------------
File: array.h
Author: Jon Diego
Date: May 2018
Brief: typedefinition of an std::array. 
Not really needed as std::array does not use allocators.
Created for coherence.
-------------------------------------------------*/
#pragma once
#include <array>

namespace mrbl
{
	template<typename T, std::size_t N>
	using array = std::array<T, N>;
}