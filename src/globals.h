#pragma once

#include "media_layer/input_system/input_system.h"//Action Creation
#include "media_layer/input_system/button_info.h"//Action Creation
#include <memory>

namespace mrbl
{
	class GameObject;
	namespace globals
	{
		namespace strings
		{
			namespace camera_movement
			{
				const char * const TurnUp = "TurnUp_Cam";
				const char * const TurnLeft = "TurnLeft_Cam";
				const char * const TurnDown = "TurnDown_Cam";
				const char * const TurnRight = "TurnRight_Cam";
				const char * const Forward = "Forward_Cam";
				const char * const Backward = "Backward_Cam";
				const char * const Up = "Up_Cam";
				const char * const Down = "Down_Cam";
				const char * const Left = "Left_Cam";
				const char * const Right = "Right_Cam";
			}
		}
		void CreateInputActions();
		void CreateGlobalObjects();
		extern std::unique_ptr<GameObject> camera_ptr;
		extern std::unique_ptr<GameObject> box_ptr;
	}
}

