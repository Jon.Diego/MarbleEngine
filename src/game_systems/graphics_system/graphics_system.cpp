/*-------------------------------------------------
File: graphics_system.cpp
Author: Jon Diego
Date: August 2018
Brief: The graphics system is both a wrapper for
The graphics API functionality and also holds the
game specific code in its Init and Update functions.
-------------------------------------------------*/
#include "graphics_system.h"
#include "exception/exception.h"
#include "logger/logger.h"
#include "GL/gl_core_4_4.hpp"
#include "resource_manager/shaders/first_shader/fist_shader.h"
#include "resource_manager/meshes/mesh.h"
#include "resource_manager/resource_manager.h"
#include "transform/transform.h"
#define GLM_ENABLE_EXPERIMENTAL
#include <GLM/gtx/quaternion.hpp>


namespace mrbl
{
	namespace GLTEST
	{
		Shader* first_shader = nullptr;
		Mesh* first_mesh = nullptr;
		Transform debug_transform;
	}
	//Singleton ptr.
	GraphicsSystem* GraphicsSystem::s_system = nullptr;

	//CONSTRUCTORS
	GraphicsSystem::GraphicsSystem()
	{
		if (s_system != nullptr)
			throw(Exception("Tried to instantiate a second Graphics System."));
		s_system = this;
	}

	GraphicsSystem::~GraphicsSystem()
	{
		if (s_system != this)
			Logger::get().write("Graphics System pointer was changed before deletion");
		else
			s_system = nullptr;
	}

	//GAME DEPENDANT

	void GraphicsSystem::init()
	{
		gl::ClearColor(0.f, 0.f, 0.f, 1.f);
		gl::Enable(gl::CULL_FACE);

		//Create the shader.
		GLTEST::first_shader = Shader::load_shader<FirstShader>("dummy_shader", "assets/glshaders/first_shader.vts", "", "", "", "assets/glshaders/first_shader.fts", "");
		//Create the mesh.
		GLTEST::first_mesh = Mesh::load_mesh("dummy_mesh",
			{
			//Face 0
			-0.5f, -0.5f, 0.5f, 1.f, 0.f, 0.f, 1.f,
			0.5f, -0.5f,  0.5f, 1.f, 0.f, 0.f, 1.f,
			-0.5f,  0.5f, 0.5f, 1.f, 0.f, 0.f, 1.f,
			 0.5f, -0.5f, 0.5f, 1.f, 0.f, 0.f, 1.f,
			 0.5f,  0.5f, 0.5f, 1.f, 0.f, 0.f, 1.f,
			-0.5f,  0.5f, 0.5f, 1.f, 0.f, 0.f, 1.f,
			//Face 1
			0.5f,  0.5f, 0.5f, 0.f, 1.f, 0.f, 1.f,
			0.5f, -0.5f, 0.5f, 0.f, 1.f, 0.f, 1.f,
			0.5f, -0.5f,-0.5f, 0.f, 1.f, 0.f, 1.f,
			0.5f, -0.5f,-0.5f, 0.f, 1.f, 0.f, 1.f,
			0.5f,  0.5f,-0.5f, 0.f, 1.f, 0.f, 1.f,
			0.5f,  0.5f, 0.5f, 0.f, 1.f, 0.f, 1.f,
			//Face 2
			0.5f,  0.5f,  -0.5f, 0.f, 0.f, 1.f, 1.f,
			0.5f, -0.5f,  -0.5f, 0.f, 0.f, 1.f, 1.f,
			-0.5f, -0.5f, -0.5f, 0.f, 0.f, 1.f, 1.f,
			-0.5f, -0.5f, -0.5f, 0.f, 0.f, 1.f, 1.f,
			-0.5f,  0.5f, -0.5f, 0.f, 0.f, 1.f, 1.f,
			 0.5f,  0.5f, -0.5f, 0.f, 0.f, 1.f, 1.f,
			//Face 3
			-0.5f,  0.5f,-0.5f, 1.f, 1.f, 0.f, 1.f,
			-0.5f, -0.5f,-0.5f, 1.f, 1.f, 0.f, 1.f,
			-0.5f, -0.5f, 0.5f, 1.f, 1.f, 0.f, 1.f,
			-0.5f, -0.5f, 0.5f, 1.f, 1.f, 0.f, 1.f,
			-0.5f,  0.5f, 0.5f, 1.f, 1.f, 0.f, 1.f,
			-0.5f,  0.5f,-0.5f, 1.f, 1.f, 0.f, 1.f,
			//Face 4
			-0.5f,  0.5f, -0.5f, 1.f, 0.f, 1.f, 1.f,
			-0.5f,  0.5f,  0.5f, 1.f, 0.f, 1.f, 1.f,
			 0.5f,  0.5f,  0.5f, 1.f, 0.f, 1.f, 1.f,
			 0.5f,  0.5f,  0.5f, 1.f, 0.f, 1.f, 1.f,
			 0.5f,  0.5f, -0.5f, 1.f, 0.f, 1.f, 1.f,
			-0.5f,  0.5f, -0.5f, 1.f, 0.f, 1.f, 1.f,
			//Face 5
			-0.5f, -0.5f, 0.5f, 0.f, 1.f, 1.f, 1.f,
			-0.5f, -0.5f,-0.5f, 0.f, 1.f, 1.f, 1.f,
			 0.5f, -0.5f,-0.5f, 0.f, 1.f, 1.f, 1.f,
			 0.5f, -0.5f,-0.5f, 0.f, 1.f, 1.f, 1.f,
			 0.5f, -0.5f, 0.5f, 0.f, 1.f, 1.f, 1.f,
			-0.5f, -0.5f, 0.5f, 0.f, 1.f, 1.f, 1.f
			}
		    ,{ 3u , 4u });
		GLTEST::debug_transform.set_rotation(glm::rotate(GLTEST::debug_transform.get_rotation(), glm::radians(180.f), glm::fvec3(0.f, 1.f, 0.f)));
		GLTEST::debug_transform.set_rotation(glm::rotate(GLTEST::debug_transform.get_rotation(), glm::radians(-90.f), glm::fvec3(1.f, 0.f, 0.f)));
	}

	void GraphicsSystem::update()
	{
		gl::Clear(gl::COLOR_BUFFER_BIT);
		Mesh* my_mesh = ResourceManager::get().get_map<Mesh>().find<Mesh>("dummy_mesh");
		ResourceManager::get().get_map<Shader>().find<Shader>("dummy_shader")->render(std::make_pair(my_mesh, &GLTEST::debug_transform));
	}

	//STATIC GETTOR
	GraphicsSystem& GraphicsSystem::get()
	{
		return *s_system;
	}
}
