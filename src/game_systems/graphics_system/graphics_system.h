/*-------------------------------------------------
File: graphics_system.h
Author: Jon Diego
Date: August 2018
Brief: The graphics system is both a wrapper for 
holds the game specific graphics code 
in its Init and Update functions.
-------------------------------------------------*/
#pragma once

#include <GLM/vec4.hpp>
#include <GLM/gtc/type_precision.hpp>


namespace mrbl
{
	class GraphicsSystem
	{
	public:

		//Constructor destructor
		GraphicsSystem();
		~GraphicsSystem();
		GraphicsSystem(const GraphicsSystem& other) = delete;
		GraphicsSystem(GraphicsSystem&& other) = delete;
		GraphicsSystem& operator =(const GraphicsSystem& other) = delete;
		GraphicsSystem& operator =(GraphicsSystem&& other) = delete;

		//Game Dependent
		void init();
		void update();

		//Static gettor.
		static GraphicsSystem& get();

	private:

		//Singleton ptr
		static GraphicsSystem* s_system;
	};
}

