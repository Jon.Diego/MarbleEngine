#pragma once
#include "containers/string.h"
#include "exception/exception.h"

namespace mrbl
{
	template<typename T>
	mrbl::string enum_to_string(T)
	{
		mrbl::Exception("Enum class needs specialization. Use Printable Enum Macro.");
	}

	void tokenize_printable_enum(size_t argument, mrbl::string & phrase);


	//Should go in .cpp, prototype a normal enum class in the .h
	#define PRINTABLE_ENUM_CLASS(X,...) \
	enum class X\
	{\
		__VA_ARGS__\
	}; \
	template<>\
	mrbl::string enum_to_string<X>(X value)\
	{\
		mrbl::string phrase(#__VA_ARGS__); \
		tokenize_printable_enum(static_cast<size_t>(value), phrase);\
		return phrase; \
	}
}