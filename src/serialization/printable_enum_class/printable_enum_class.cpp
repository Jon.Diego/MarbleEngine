/* Start Header -------------------------------------------------------
Copyright (C) 2017 DigiPen Institute of Technology. Reproduction or disclosure of this file or its contents without the prior written consent of DigiPen Institute of Technology is prohibited.
File Name: PrintableEnumClass.hpp
Purpose: An enum class that can be printed and serialized. To be used when the enum is to be shown in the GUI
Author: Jon Diego jon.diego 540001914
Creation date: September 2017
- End Header --------------------------------------------------------*/
#include "printable_enum_class.h"

namespace mrbl
{
	void tokenize_printable_enum(size_t argument, mrbl::string & phrase)
	{
		size_t  start = 0;
		for (size_t i = 0; i < phrase.length(); ++i)
		{
			if (argument == 0)
			{
				start = i;
				break;
			}
			if (phrase[i] == ',')
			{
				--argument;
			}
		}
		if (argument)
		{
			phrase = "UNKNOWN_VALUE";
			return;
		}
		size_t end = phrase.length();
		for (size_t  i = start; i < end; ++i)
		{
			if (phrase[i] == ',')
			{
				end = i;
				break;
			}
		}
		while (start < end)
		{
			if (phrase[start] != ' ')
				break;
			++start;
		}
		while (start < end)
		{
			if (phrase[end - 1] != ' ')
				break;
			--end;
		}
		if (start < end)
			phrase = phrase.substr(start, end - start);
		else
			phrase.clear();
	}
}