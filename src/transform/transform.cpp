/*-------------------------------------------------
File: transform.cpp
Author: Jon Diego
Date: June 2018
Brief: 3D transform class, Used to represent the
scale rotation and translation of elements in
a coordinate system.
-------------------------------------------------*/
#include "transform.h"
#define GLM_ENABLE_EXPERIMENTAL
#include <GLM/gtx/transform.hpp>//glm::scale, glm::translate
#include <GLM/gtx/quaternion.hpp>//glm::toMat4

namespace mrbl
{
	/***********************************************************/
	//Legend:
	//	TRS: Translate, Rotate,Scale matrix. The matrix is supposed to premultiply the points.
	/**********************************************************/

	/*--------------------------------------
	brief: Constructor, creates and populates the Transform Class.
	parameters:
		position: The translation of the object in local space coordinates.
		scale: The scale of the object in local space coordinates.
		rotation: Rotation of the object in local scale coordinates.
		parent: Pointer to the parent transform if there is any.
	--------------------------------------*/
	Transform::Transform(glm::fvec3 position, glm::fvec3 scale, glm::fquat rotation, Transform* parent)
		: m_position(std::move(position)), m_scale(std::move(scale)), m_rotation(std::move(rotation)),m_parent(parent)
	{}

	/*--------------------------------------
	brief: Copy constructor.
	parameters:
		other: The transform to copy.
	--------------------------------------*/
	Transform::Transform(const Transform & other)
		: m_position(other.m_position), m_scale(other.m_scale), m_rotation(other.m_rotation), m_parent(other.m_parent)
	{}

	/*--------------------------------------
	brief: Move constructor.
	parameters:
		other: The transform to Move.
	--------------------------------------*/
	Transform::Transform(Transform && other)
		: m_position(other.m_position), m_scale(other.m_scale), m_rotation(other.m_rotation), m_parent(other.m_parent)
	{}

	/*--------------------------------------
	brief: Assingment Operator
	parameters:
		other: The transform to copy.
	return:
		A reference to itself.
	--------------------------------------*/
	Transform& Transform::operator= (const Transform &other)
	{
		m_position = other.m_position;
		m_scale = other.m_scale;
		m_rotation = other.m_rotation;
		m_parent = other.m_parent;
		return *this;
	}

	/*--------------------------------------
	brief: Move Operator
	parameters:
	other: The transform to move.
	return:
		A reference to itself.
	--------------------------------------*/
	Transform& Transform::operator= (Transform && other)
	{
		m_position = std::move(other.m_position);
		m_scale = std::move(other.m_scale);
		m_rotation = std::move(other.m_rotation);
		m_parent = std::move(other.m_parent);
		return *this;
	}

	/*--------------------------------------
	brief: Returns the translation in local space coordinates.
	return:
		Const Reference to the position.
	--------------------------------------*/
	const glm::fvec3 & Transform::get_position() const
	{
		return m_position;
	}

	/*--------------------------------------
	brief: Returns the scale in local space coordinates.
	return:
	Const Reference to the scale..
	--------------------------------------*/
	const glm::fvec3 & Transform::get_scale() const
	{
		return m_scale;
	}

	/*--------------------------------------
	brief: Returns the rotation in local space coordinates.
	return:
	Const Reference to the rotation quaternion.
	--------------------------------------*/
	const glm::fquat & Transform::get_rotation() const
	{
		return m_rotation;
	}

	/*--------------------------------------
	brief: Returns the parent transform.
	return:
		Pointer to the parent transform.Can be null if there is no parent.
	--------------------------------------*/
	Transform* Transform::get_parent() const
	{
		return m_parent;
	}

	/*--------------------------------------
	brief: Sets the position in local space coordinates.
	parameters:
		position: The position to set.
	--------------------------------------*/
	void Transform::set_position(glm::fvec3 position)
	{
		m_rotation = std::move(position);
	}

	/*--------------------------------------
	brief: Sets the scale in local space coordinates.
	parameters:
		scale: The scale to set.
	--------------------------------------*/
	void Transform::set_scale(glm::fvec3 scale)
	{
		m_scale = std::move(scale);
	}

	/*--------------------------------------
	brief: Sets the rotation in local space coordinates.
	parameters:
		rotation: The rotation to set.
	--------------------------------------*/
	void Transform::set_rotation(glm::fquat rotation)
	{
		m_rotation = std::move(rotation);
	}

	/*--------------------------------------
	brief: Sets the current transform's parent.
	parameters:
		parent: The parent transform to set.
	--------------------------------------*/
	void Transform::set_parent(Transform* parent)
	{
		m_parent = parent;
	}

	/*--------------------------------------
	brief: Returns the local TRS matrix
	return:
		The local TRS Matrix.
	--------------------------------------*/
	glm::fmat4 Transform::get_local_matrix() const
	{
		return get_local_matrix_no_scale() * glm::scale(m_scale);
	}

	/*--------------------------------------
	brief: Returns the local TR matrix
	return:
		The local TR Matrix.
	--------------------------------------*/
	glm::fmat4 Transform::get_local_matrix_no_scale() const
	{
		return glm::translate(m_position) * glm::toMat4(m_rotation);
	}

	/*--------------------------------------
	brief: Returns the inverse of the local TRS matrix
	return:
		The inverted local TRS Matrix.
	--------------------------------------*/
	glm::fmat4 Transform::get_local_inv_matrix() const
	{
		return glm::scale(1.f / m_scale) * get_local_inv_matrix_no_scale();
	}

	/*--------------------------------------
	brief: Returns the inverse of the local TR matrix
	return:
		The inverted, local TR Matrix.
	--------------------------------------*/
	glm::fmat4 Transform::get_local_inv_matrix_no_scale() const
	{
		return glm::toMat4(glm::fquat(m_rotation.w, m_rotation.x, m_rotation.y, m_rotation.z)) * glm::translate(-m_position);
	}

	/*--------------------------------------
	brief: Returns a compound TRS matrix that positions the object in the space of its uppermost parent is located in.
	return:
	The world matrix.
	--------------------------------------*/
	glm::fmat4 Transform::get_world_matrix()const
	{
		Transform* parent = get_parent();
		if (parent)
			return parent->get_world_matrix() * get_local_matrix();
		else
			return get_local_matrix();
	}

	/*--------------------------------------
	brief: Returns a compound TR matrix that positions the object in the space of its uppermost parent is located in.
	return:
	The world matrix without scale.
	--------------------------------------*/
	glm::fmat4 Transform::get_world_matrix_no_scale()const
	{
		Transform* parent = get_parent();
		if (parent)
			return parent->get_world_matrix_no_scale() * get_local_matrix_no_scale();
		else
			return get_local_matrix_no_scale();
	}

	/*--------------------------------------
	brief: Returns a compound TRS matrix that positions the object in the space of its uppermost parent is located in.
	       The object uses its own scale but ignores that of the parents.
	return:
	The world matrix without the parent's scale.
	--------------------------------------*/
	glm::fmat4 Transform::get_world_matrix_no_parent_scale()const
	{
		Transform* parent = get_parent();
		if (parent)
			return parent->get_world_matrix_no_scale() * get_local_matrix();
		else
			return get_local_matrix();
	}

	/*--------------------------------------
	brief: Returns a compound TRS matrix that brings objects from the space where their uppermost parent is located to the object's local space.
	return:
	The inverted world matrix.
	--------------------------------------*/
	glm::fmat4 Transform::get_world_inv_matrix()const
	{
		Transform* parent = get_parent();
		if (parent)
		{
			glm::mat4 parent_mat = parent->get_world_inv_matrix();
			return get_local_inv_matrix() * parent_mat;
		}
		return get_local_inv_matrix();
	}

	/*--------------------------------------
	brief: Returns a compound TRS matrix that brings objects from the space where their uppermost parent is located to the object's local space.
	This version ignores all the scales.
	return:
	The inverted world matrix without any scale.
	--------------------------------------*/
	glm::fmat4 Transform::get_world_inv_matrix_no_scale()const
	{
		Transform* parent = get_parent();
		if (parent)
		{
			glm::mat4 parent_mat = parent->get_world_inv_matrix_no_scale();
			return get_local_inv_matrix_no_scale() * parent_mat;
		}
		return get_local_inv_matrix_no_scale();

	}

	/*--------------------------------------
	brief: Returns a compound TRS matrix that brings objects from the space where their uppermost parent is located to the object's local space.
	       This version ignores all the scales but the one of the current object.
	return:
	The inverted world matrix without the parent's scale.
	--------------------------------------*/
	glm::fmat4 Transform::get_world_inv_matrix_no_parent_scale()const
	{
		Transform* parent = get_parent();
		if (parent)
		{
			glm::mat4 parent_mat = parent->get_world_inv_matrix_no_scale();
			return get_local_inv_matrix() * parent_mat;
		}
		return get_local_inv_matrix();
	}
}