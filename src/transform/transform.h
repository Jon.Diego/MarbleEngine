/*-------------------------------------------------
File: transform.h
Author: Jon Diego
Date: June 2018
Brief: 3D transform class, Used to represent the
scale rotation and translation of elements in
a coordinate system.
-------------------------------------------------*/
#pragma once
#include <GLM/vec3.hpp>
#include <GLM/gtc/quaternion.hpp>
#include <GLM/mat4x4.hpp>

namespace mrbl
{	
	class Transform
	{
	public:
		//CONSTRUCTORS AND Asingment operations.
		Transform(glm::fvec3 position = glm::fvec3(0.f, 0.f, 0.f), glm::fvec3 scale = glm::fvec3(1.f, 1.f, 1.f), glm::fquat rotation = glm::fquat(1.f, 0.f, 0.f, 0.f), Transform * parent = nullptr);
		~Transform() = default;
		Transform(const Transform &other);
		Transform(Transform &&other);
		Transform& operator= (const Transform &other);
		Transform& operator= (Transform && other);

		//GETTORS 
		const glm::fvec3& get_position()const;
		const glm::fvec3& get_scale()const;
		const glm::fquat& get_rotation()const;
		Transform* get_parent()const;

		//SETTORS
		void set_position(glm::fvec3 position);
		void set_scale(glm::fvec3 scale);
		void set_rotation(glm::fquat rotation);
		void set_parent(Transform* parent);

		//MATRIX GETTORS
		//Local coordinate system.
		glm::fmat4 get_local_matrix() const;
		glm::fmat4 get_local_matrix_no_scale()const;
		glm::fmat4 get_local_inv_matrix()const;
		glm::fmat4 get_local_inv_matrix_no_scale()const;
		//In the topmost parent coordinate system.
		glm::fmat4 get_world_matrix() const;
		glm::fmat4 get_world_matrix_no_scale()const;
		glm::fmat4 get_world_matrix_no_parent_scale()const;
		glm::fmat4 get_world_inv_matrix()const;
		glm::fmat4 get_world_inv_matrix_no_scale()const;
		glm::fmat4 get_world_inv_matrix_no_parent_scale()const;

	private:
		glm::fvec3 m_position;
		glm::fvec3 m_scale;
		glm::fquat m_rotation;
		Transform* m_parent;
	};
}