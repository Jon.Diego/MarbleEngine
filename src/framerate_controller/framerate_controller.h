/*-------------------------------------------------
File: framerate_controller.h
Author: Jon Diego
Date: Jun 2018
Brief: Framerate controller, allows the user to know
the current delta time and also to set a minimum 
and maximum delta times.
-------------------------------------------------*/
#pragma once
#include <chrono> //std::chrono
#include <utility>//std::pair
#include <limits> //std::numeric_limits

namespace mrbl
{
	class FramerateController
	{
	public:
		//Constructor, Has default values for min_dt and max_dt.
		FramerateController(float initial_dt, float min_dt = 0.f, float max_dt = std::numeric_limits<float>::max());
		//Destructor, sets the singleton pointer to nullptr.
		~FramerateController();
		//ACCESS
		//Returns the Framerate controller.
		static FramerateController& get();

		//CONVERSORS
		//Conversion from delta to frames and viceversa.
		static float delta_to_frames(float dt);
		static float frames_to_delta(float frames);

		//Settors.
		void set_max_dt(float max_dt);
		void set_min_dt(float min_dt);

		//Gettors
		float get_dt()const;
		float get_fps()const;
		float get_min_dt()const;
		float get_min_fps()const;
		float get_max_dt()const;
		float get_max_fps()const;
		

		//Waits until the minimum dt has already passed.
		std::pair<float, std::chrono::steady_clock::time_point> wait_min_dt();
		//Calculates the delta to use in the next frame. Waits for the minimum dt to pass first.
		float end_frame();

	private:
		//Singleton ptr.
		static FramerateController* s_controller;
		float m_dt_min;//minimum delta time that has to pass to start a new frame.
		float m_dt_max;//maximum delta time that we are willing to accept before slowing down the game for stability.
		float m_dt;//The current delta time.
		std::chrono::steady_clock::time_point m_last_frame;//The time stored when the last delta time was calculated.
	};
}