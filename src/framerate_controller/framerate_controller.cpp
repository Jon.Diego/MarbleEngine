/*-------------------------------------------------
File: framerate_controller.h
Author: Jon Diego
Date: Jun 2018
Brief: Framerate controller, allows the user to know
the current delta time and also to set a minimum
and maximum delta times.
-------------------------------------------------*/
#include "framerate_controller.h"
#include "exception/exception.h"
#include "logger/logger.h"
#include <algorithm> //std::min
#include <thread> //std::this_thread::yield

namespace mrbl
{
	//Singleton ptr.
	FramerateController* FramerateController::s_controller = nullptr;

	/*--------------------------------------
	brief: Constructor, Has default values for min_dt and max_dt.
		   May throw an exception if another Framerate Controller already exists.
	parameters:
		initial_dt: The dt to use during the first frame.
		min_dt: minimum dt(max framerate) that we are willing to accept, will wait if game runs faster.
		max_dt: max dt (min framerate) that we are willing to accept before slowing down the game.
	--------------------------------------*/
	FramerateController::FramerateController(float initial_dt, float min_dt, float max_dt):
		m_dt_min(min_dt),m_dt_max(max_dt), m_dt(initial_dt), m_last_frame(std::chrono::steady_clock::now())
	{
		if(s_controller != nullptr)
			throw(Exception("Tried to instantiate a second Framerate Controller."));
		s_controller = this;
	}

	/*--------------------------------------
	brief: Destructor, sets the singleton pointer to nullptr.
	--------------------------------------*/
	FramerateController::~FramerateController()
	{
		if (s_controller != this)
			Logger::get().write("Framerate controller pointer was changed before deletion");
		else
			s_controller = nullptr;
	}


	/*--------------------------------------
	brief: Returns the Framerate controller.
	return:
		Reference to the framerate controller instance.
	--------------------------------------*/
	FramerateController & FramerateController::get()
	{
		return *s_controller;
	}


	/*--------------------------------------
	brief: Converts a delta time to a number of frames per seconds.
	parameters:
		dt: The delta time to convert.
	return:
		The equivalent number of frames.
	--------------------------------------*/
	float FramerateController::delta_to_frames(float dt)
	{
		return 1.f/dt;
	}

	/*--------------------------------------
	brief: Converts a number of frames per second to an equivalent delta time.
	parameters:
		frames: The number of frames to convert.
	return:
		The equivalent delta time.
	--------------------------------------*/
	float FramerateController::frames_to_delta(float frames)
	{
		return delta_to_frames(frames);
	}


	/*--------------------------------------
	brief: Sets the maximum allowed dt. If the actual dt is bigger a slowdown will happen.
	parameters:
		max_dt: The new max_dt
	--------------------------------------*/
	void FramerateController::set_max_dt(float max_dt)
	{
		m_dt_max = max_dt; 
	}

	/*--------------------------------------
	brief: Sets the minimum allowed dt. If the actual dt is smaller, the process will wait until it isn�t.
	parameters:
	max_dt: The new min_dt
	--------------------------------------*/
	void FramerateController::set_min_dt(float min_dt)
	{
		m_dt_min = min_dt;
	}

	/*--------------------------------------
	brief: Returns the dt
	return:
		the value of m_dt
	--------------------------------------*/
	float FramerateController::get_dt()const
	{
		return m_dt;
	}

	/*--------------------------------------
	brief: Returns the current fps
	return:
		The current fps.
    --------------------------------------*/
	float FramerateController::get_fps()const
	{
		return delta_to_frames(m_dt);
	}

	/*--------------------------------------
	brief: Returns the min dt.
	return:
		the value of m_dt_min
	--------------------------------------*/
	float FramerateController::get_min_dt()const
	{
		return m_dt_min;
	}


	/*--------------------------------------
	brief: Returns the minimun fps allowed before slowdown.
	return:
		minimun fps allowed before slowdown.
	--------------------------------------*/
	float FramerateController::get_min_fps()const
	{
		return delta_to_frames(m_dt_max);
	}

	/*--------------------------------------
	brief: Returns the max dt.
	return:
		the value of m_dt_max
	--------------------------------------*/
	float FramerateController::get_max_dt()const
	{
		return m_dt_max;
	}

	/*--------------------------------------
	brief: Returns the max dt.
	return:
		maximum fps allowed before making the game_engine wait.
	--------------------------------------*/
	float FramerateController::get_max_fps()const
	{
		return delta_to_frames(m_dt_min);
	}


	/*--------------------------------------
	brief: Waits until the time elapsed in this frame is equal or bigger to m_dt_min.
	return:
		pair.first = The delta time of this frame after the wait.
		pair.second = The time point when the wait ended.
	--------------------------------------*/
	std::pair<float, std::chrono::steady_clock::time_point> FramerateController::wait_min_dt()
	{
		std::chrono::steady_clock::time_point now = std::chrono::steady_clock::now();
		std::chrono::duration<double, std::milli> duration(now - m_last_frame);
		float new_delta = static_cast<float>(duration.count() / 1000.0);
		while (new_delta < m_dt_min)
		{
			std::this_thread::yield();
			now = std::chrono::steady_clock::now();
			duration = now - m_last_frame;
			new_delta = static_cast<float>(duration.count() / 1000.0);
		}
		return std::pair<float, std::chrono::steady_clock::time_point>(new_delta, now);
	}

	/*--------------------------------------
	brief: Waits until the time elapsed in this frame is equal or bigger to m_dt_min.
		   Then sets the dt for the next frame between the minimum after the wait and the max dt.
	return:
		   The dt for the next frame.
	--------------------------------------*/
	float FramerateController::end_frame()
	{
		std::pair<float, std::chrono::steady_clock::time_point> pair = wait_min_dt();
		m_dt = std::min(pair.first,m_dt_max);
		m_last_frame = pair.second;
		return m_dt;
	}
}